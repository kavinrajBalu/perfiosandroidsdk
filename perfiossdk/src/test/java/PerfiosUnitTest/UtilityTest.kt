package PerfiosUnitTest

import com.perfios.utils.encodeHeader
import com.perfios.utils.getCurrentDateTime
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class UtilityTest {
    @Test
    fun getCurrentTime()
    {
        assertTrue(getCurrentDateTime() !=null)
    }

    @Test
    fun encodeHeaderTest()
    {
        val data = "Perfios"
        val encodedHeader = encodeHeader(data)
        assertNotEquals(data,encodedHeader)

    }
}