package PerfiosUnitTest

import com.perfios.objects.DocType
import com.perfios.objects.InstancePool
import com.perfios.retrofit.PerfiosRetroClient
import com.perfios.retrofit.PerfiosService
import com.perfios.utils.getCurrentDateTime
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class PerfiosAPITest {

    private lateinit var perfiosService :PerfiosService

    @Before
    fun setUp() {
        perfiosService = PerfiosRetroClient.getPerfiosService()!!
    }

    @Test
  fun sendDocType_Success()
  {
      val docType = DocType("PAN","true","image uri", getCurrentDateTime())
      try {
          val response = perfiosService.sendDoctypeToPerfios(docType, "acme", InstancePool.perfiosTransactionID ?: "").execute()
          Assert.assertTrue(response.isSuccessful)
      }
      catch (e:Exception)
      {
        e.printStackTrace()
      }
  }

}