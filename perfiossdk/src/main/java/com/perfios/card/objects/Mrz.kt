package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

 data class Mrz(
    @SerializedName("line1")
    var line1 : String? = null,

    @SerializedName("line2")
    var line2 : String? = null,

    @SerializedName("conf")
    var confidence : Int? = null
)