package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

data class VoterIdFrontNew(
     @SerializedName("voterid")

    var voterid : ValueConfidence? = null,

     @SerializedName("name")

    var name : ValueConfidence? = null,

     @SerializedName("relation")

    var relation : ValueConfidence? = null,

     var tag : String? = null

    )