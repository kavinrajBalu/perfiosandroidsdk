package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

 data class Aadhaar(

    @SerializedName("value")

    var value : String? = null ,

    @SerializedName("ismasked")

    var isMasked : String? =null,

    @SerializedName("conf")

    var confidence : Int? = null
)