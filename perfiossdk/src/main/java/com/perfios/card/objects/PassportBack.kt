package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

data class PassportBack(

    @SerializedName("address")

    var address : Address? = null,


    @SerializedName("father")

    var father : ValueConfidence? = null,


    @SerializedName("mother")

    var mother : ValueConfidence? = null,

    @SerializedName("file_num")

    var fileNumber : ValueConfidence? = null,

    @SerializedName("old_doi")

    var oldDoi : ValueConfidence? = null,

    @SerializedName("old_passport_num")

    var oldPassportNumber: ValueConfidence? = null,

    @SerializedName("old_place_of_issue")

    var oldPlaceOfIssue : ValueConfidence? = null,

    @SerializedName("passport_num")

    var passportNumber : ValueConfidence? = null,

    @SerializedName("pin")

    var pin : ValueConfidence? = null,

    @SerializedName("spouse")

    var spouse : ValueConfidence? = null,

    var tag : String? = null
)