package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

data class VoterIdBack(


    @SerializedName("voterid")

    var voterid : ValueConfidence? = null,

    @SerializedName("gender")

    var gender : ValueConfidence? = null,

    @SerializedName("pin")

    var pin : ValueConfidence? = null,

    @SerializedName("dob")

    var dob : ValueConfidence? = null,

    @SerializedName("age")

    var age : ValueConfidence? = null,

    @SerializedName("date")

    var date : ValueConfidence? = null,

    @SerializedName("type")

    var type : ValueConfidence? = null,

    @SerializedName("address")

    var address : Address? = null,

    var tag : String? = null
)