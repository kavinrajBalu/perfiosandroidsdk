package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

data class AadhaarBack(

    @SerializedName("aadhaar")

    var aadhaar : Aadhaar? = null,

    @SerializedName("address")

    var address : Address? = null,

    @SerializedName("father")

    var father : ValueConfidence? = null,

    @SerializedName("husband")

    var husband : ValueConfidence? = null,

    @SerializedName("pin")

    var pin : ValueConfidence? = null,

    @SerializedName("qr")

    var qr : ValueConfidence? = null,

    var tag : String? = null)