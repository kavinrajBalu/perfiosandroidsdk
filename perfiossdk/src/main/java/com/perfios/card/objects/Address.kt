package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

 data class Address(

    @SerializedName("care_of")

    var careOf : String? = null ,


    @SerializedName("district")

    var district : String? = null ,


    @SerializedName("city")

    var city : String? = null ,


    @SerializedName("locality")

    var locality : String? = null ,


    @SerializedName("landmark")

    var landmark : String? = null ,


    @SerializedName("street")

    var street : String? = null ,


    @SerializedName("line1")

    var line1 : String? = null ,


    @SerializedName("line2")

    var line2 : String? = null ,


    @SerializedName("house_number")

    var houseNumber : String? = null ,


    @SerializedName("pin")

    var pin : String? = null ,


    @SerializedName("state")

    var state : String? = null ,


    @SerializedName("value")

    var value : String? = null ,


    @SerializedName("conf")

    var confidence : Int? = null
)