package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

 data class ValueConfidence(
     @SerializedName("value")
    
     var value : String? = null ,

     @SerializedName("conf")
    
     var confidence : Int? = null
 )
