package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

data class AadhaarFrontBottom(

    @SerializedName("aadhaar")

    var aadhar : Aadhaar? = null,

    @SerializedName("dob")

    var dob : ValueConfidence? = null,

    @SerializedName("father")

    var father : ValueConfidence? = null,

    @SerializedName("gender")

    var gender : ValueConfidence? = null,

    @SerializedName("mother")

    var mother : ValueConfidence? = null,

    @SerializedName("name")

    var name : ValueConfidence? = null,

    @SerializedName("yob")

    var yob : ValueConfidence? = null,

    @SerializedName("qr")

    var qr : ValueConfidence? = null,

    var tag: String? = null
)