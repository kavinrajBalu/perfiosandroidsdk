package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

data class OldPan(
    @SerializedName("date")

    var date : ValueConfidence? = null,

    @SerializedName("father")

    var father : ValueConfidence? = null,

    @SerializedName("name")

    var name : ValueConfidence? = null,

    @SerializedName("pan_no")

    var panNumber : ValueConfidence? = null,

    var tag: String? = null

)