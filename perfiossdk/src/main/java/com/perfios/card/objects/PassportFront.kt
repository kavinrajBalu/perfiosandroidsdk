package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

data class PassportFront(


    @SerializedName("country_code")

    var countryCode : ValueConfidence? = null,

    @SerializedName("dob")

    var dob : ValueConfidence? = null,

    @SerializedName("doe")

    var doe : ValueConfidence? = null,

    @SerializedName("doi")

    var doi : ValueConfidence? = null,

    @SerializedName("gender")

    var gender : ValueConfidence? = null,

    @SerializedName("given_name")

    var givenName : ValueConfidence? = null,

    @SerializedName("nationality")

    var nationality : ValueConfidence? = null,

    @SerializedName("passport_num")

    var passportNumber : ValueConfidence? = null,

    @SerializedName("place_of_birth")

    var placeOfBirth: ValueConfidence? = null,

    @SerializedName("place_of_issue")

    var placeOfIssue: ValueConfidence? = null,

    @SerializedName("surname")

    var surname: ValueConfidence? = null,

    @SerializedName("mrz")

    var mrz: Mrz? = null,

    @SerializedName("type")

    var type: ValueConfidence? = null,

    var tag : String? = null
    )