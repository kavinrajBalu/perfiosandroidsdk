package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

data class VoterIdFront(
     @SerializedName("voterid")

    var voterid : ValueConfidence? = null,

     @SerializedName("name")

    var name : ValueConfidence? = null,

     @SerializedName("gender")

    var gender : ValueConfidence? = null,

     @SerializedName("relation")

    var relation : ValueConfidence? = null,

     @SerializedName("dob")

    var dob : ValueConfidence? = null,

     @SerializedName("doc")

    var doc: ValueConfidence? = null,

     @SerializedName("age")

    var age : ValueConfidence? = null,

     var tag : String? = null
)