package com.perfios.card.objects

import com.google.gson.annotations.SerializedName

data class AadhaarFrontTop(
    @SerializedName("aadhaar")

    var aadhaar : Aadhaar? = null,

    @SerializedName("address")

    var address : Address? = null,

    @SerializedName("father")

    var father : ValueConfidence? = null,

    @SerializedName("husband")

    var husband : ValueConfidence? = null,

    @SerializedName("name")

    var name : ValueConfidence? = null,

    @SerializedName("phone")

    var phone : ValueConfidence? = null,

    @SerializedName("pin")

    var pin : ValueConfidence? = null,

    var tag : String? = null

    )