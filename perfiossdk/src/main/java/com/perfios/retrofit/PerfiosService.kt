package com.perfios.retrofit

import com.perfios.objects.DocType
import com.perfios.objects.LoginRequest
import com.perfios.objects.LoginResponse
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * All the perfios api request method's available in this interface.
 *
 */
 internal interface PerfiosService {

    /**
    * API 1 :Login API
     * @param loginRequest : pass the login request object.
     * Example request object:({"clientId":"7e8d5a41e4431607453451752bcd3dbd487e2e2e1d91728e0e37e8c0a657c7846754052f4b381429afacc46af615c86456",
     * "clientTransactionId":"e2c93e77-bb5b-4d89-aab4-e25dc728990b","deviceID":"",
     * "secret":"d624f6cba886585ceceb84ef5bb7e35fadeb79999090195fe6e74e7db9b186aeb4b9fa8acae8e369b43a181b8b5890ea2f418537fe","timestamp":"28-10-2019 12:24:15 GMT+05:30"})
     * @param organization : send the organization name registered with the perfios.
    * */
    @POST("start/{organization}")
    fun login(@Body loginRequest: LoginRequest, @Path("organization") organization:String): Call<LoginResponse>




    /**
    * API 2 :update selected document to perfios database
     * @param docType : document type selected by the user Eg.(AADHAR,PAN,PASSPORT,VOTERID)
     * @param perfiosTransactionID : Perfios transaction ID received from the login request.
     * @param organization : send the organization name registered with the perfios.
    * */
    @POST("update/{organization}/{perfiosTransactionID}")
    fun sendDoctypeToPerfios(@Body docType: DocType, @Path("organization")organization:String, @Path("perfiosTransactionID")perfiosTransactionID:String):Call<Void>





    /**
    * API 3 : Send OCR data copy to perfios data base
     * @param organization : send the organization name registered with the perfios.
     * @param perfiosTransactionID : Perfios transaction ID received from the login request.
     *
    * */
    @POST("postprocess/{organisation}/{perfiosTransactionId}")
    fun sendFinalData(@Body requestBody: RequestBody, @Path("organisation")organization: String, @Path("perfiosTransactionId")perfiosTransactionID:String):Call<Void>
}