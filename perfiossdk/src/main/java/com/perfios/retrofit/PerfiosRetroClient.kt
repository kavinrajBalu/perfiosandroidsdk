package com.perfios.retrofit

import okhttp3.OkHttpClient
import okhttp3.Request
import okio.Buffer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * perfios retrofit client class.
 *
 */
 internal class PerfiosRetroClient
{
    companion object {
        private const val BASE_URL = "https://apidemo02.perfios.com/KYCServer/api/verification/v1/ocrmobile/"
        private const val SIGNATURE = "signature"
        private  var retrofit: Retrofit? = null
        fun getClient(): Retrofit? {
            if (retrofit == null) {
                val httpClient = OkHttpClient.Builder()
                httpClient.addInterceptor { chain ->
                    val original = chain.request()
                    
//                    val uri = original.url().uri().toString()

                    val buffer = Buffer()

                    original.body()?.writeTo(buffer)

                    val request: Request? =  original.newBuilder()
                        .header(SIGNATURE, com.perfios.utils.encodeHeader(buffer.readUtf8()))
                        .build()
                    chain.proceed(request)
                }

                val client = httpClient.build()

                retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build()
            }
            return retrofit
        }

        fun getPerfiosService() :PerfiosService?
        {
            return getClient()?.create(PerfiosService::class.java)
        }
    }
}