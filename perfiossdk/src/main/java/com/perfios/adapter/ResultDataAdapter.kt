package com.perfios.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.perfios.perfiossdk.R
import kotlinx.android.synthetic.main.list_item.view.*
import java.util.*

/**
 * TODO Result Adapter is common for all the result screens.
 *
 * @property context : Context of the current activity.
 * @property items : hashmap items to be displayed on the result screen.
 */
internal class ResultDataAdapter(val context:Context, private val items : LinkedHashMap<String, String>) : RecyclerView.Adapter<ResultDataAdapter.ViewHolder>() {
    private var keys:Array<String> = emptyArray()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
        keys = items.keys.toTypedArray()
       return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = keys[position]
        holder.value.text = if(items[keys[position]].isNullOrBlank()) context.getString(R.string.empty_value_alternator) else items[keys[position]]
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.name!!
        val value = view.value!!
    }
}