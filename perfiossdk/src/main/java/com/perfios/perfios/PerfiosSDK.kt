package com.perfios.perfios

import android.content.Context
import android.content.Intent
import android.util.Log
import co.hyperverge.hypersnapsdk.HyperSnapSDK
import co.hyperverge.hypersnapsdk.activities.HVDocsActivity
import co.hyperverge.hypersnapsdk.activities.HVFaceActivity
import co.hyperverge.hypersnapsdk.network.HVNetworkHelper
import co.hyperverge.hypersnapsdk.objects.HVDocConfig
import co.hyperverge.hypersnapsdk.objects.HVFaceConfig
import co.hyperverge.hypersnapsdk.objects.HyperSnapParams.Region
import com.google.gson.Gson
import com.perfios.interfaces.FaceMatchResultCallBack
import com.perfios.interfaces.KYCResultCallBack
import com.perfios.interfaces.LocationCallBack
import com.perfios.interfaces.ResultCallBack
import com.perfios.objects.*
import com.perfios.perfiossdk.R
import com.perfios.retrofit.PerfiosRetroClient
import com.perfios.retrofit.PerfiosService
import com.perfios.screens.*
import com.perfios.screens.FaceMatchResult
import com.perfios.screens.LivenessResult
import com.perfios.utils.*
import okhttp3.RequestBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException


/**
 *Perfios SDK is the core class where all the perfios transaction is monitored.
 */

  internal class PerfiosSDK {

    // From login response PerfiosTransactionID  is used to make further request with perfios.
    var loginResponse: LoginResponse? = null

    // Front document URI is used to make faceMatch API call.
    lateinit var frontDocumentImageURI: String

    private lateinit var documentImageURI: String

    // Based on the document selection OCR end point selected
    private var ocrEndPoint: String? = null

    // perfios api end points available in this class
    private val perfioService: PerfiosService = PerfiosRetroClient.getPerfiosService()!!

    //clubbedResponse contains both front and backside OCR of the document.
    var clubbedResponse: JSONArray = JSONArray()

    //Final object shared to perfios.Object contain OCR data and Face match result.
    var finalPerfiosJsonObject = JSONObject()

    //FaceMatch result object
    var faceMatchJsonObject = JSONObject()

    // Document type
    lateinit var documentType: DocumentType

    //Instruction screen messages configured using doc config.
    lateinit var docConfig: HVDocConfig

    //context form clinet app.
    private lateinit var context: Context

    //face capture image uri
    lateinit var faceImageURI: String

    //json object params for face capture
    private lateinit var jsonObjectParams: JSONObject

    //Liveness result object
    var liveness: JSONObject? = null


    /**
     * Initialize SDK
     * @param context : client context
     * @param appId : appId received from the perfios.
     * @param appKey : appKey received from the perfios.
     * @param region : Current region. getLocale method in utility class helps to get the current region name
     */
    private fun init(context: Context, appId: String, appKey: String, region: Region) {
        HyperSnapSDK.init(context, appId, appKey, region)
    }


    /**
     * @param context context of the current activity
     * @param hvconfig add customization with hvfaceConfig object
     */

    private fun startFaceCaptureActivity(context: Context, hvconfig: HVFaceConfig) {
        HVFaceActivity.start(context, hvconfig) { error, result, _ ->
            if (error != null) {
                if(error.errorMessage.contains(context.getString(R.string.host_error)) || error.errorMessage.contains("I/O"))
                {
                    InstancePool.kycResultCallBack?.onFailed(context.getString(R.string.no_network_error), null)
                }
                else if(error.errorMessage.contains(context.getString(R.string.camera_permission_error)))
                {
                    InstancePool.kycResultCallBack?.onFailed(context.getString(R.string.camera_permission_denied), null)
                }
                else
                {
                    InstancePool.kycResultCallBack?.onFailed(error.errorMessage, null)
                }

            } else {
                faceImageURI = result.getString(context.getString(R.string.perfios_image_uri))
                jsonObjectParams = JSONObject()
                jsonObjectParams.put(
                    context.getString(R.string.type),
                    context.getString(R.string.id)
                )


                if (loginResponse?.isFaceMatch!! && loginResponse?.isLiveness!! || !loginResponse?.isFaceMatch!! && loginResponse?.isLiveness!!) {
                    liveness = result
                    finalPerfiosJsonObject.put(context.getString(R.string.livenessData_key), result)
                }


                //TODO : Only liveness then show liveness result intermediate screen.
                if (!loginResponse?.isFaceMatch!! && loginResponse?.isLiveness!! && loginResponse?.retry!! > 0) {
                    //TODO : if faceMatchLiveness true then don't show intermediate screen.
                    if (loginResponse?.showSelfieResultScreen!!) {
                        showLivenessResultScreen(context, result)

                    } else {
                        sendFinalData(context, finalPerfiosJsonObject)
                        InstancePool.kycResultCallBack?.onSuccess(
                            clubbedResponse,
                            null,
                            liveness,
                            faceImageURI
                        )
                    }
                    //sendFinalData(context, finalPerfiosJsonObject)
                }
                //TODO : only liveness without intermediate screen.
                else if (!loginResponse?.isFaceMatch!! && loginResponse?.isLiveness!! && loginResponse?.retry!! == 0) {
                    sendFinalData(context, finalPerfiosJsonObject)
                    InstancePool.kycResultCallBack?.onSuccess(
                        clubbedResponse,
                        null,
                        liveness,
                        faceImageURI
                    )
                } else if (loginResponse?.isFaceMatch!!) {
                    if (loginResponse?.retry!! > 0) {
                        //TODO : if faceMatchLiveness true then don't show intermediate screen.
                        if (loginResponse?.showSelfieResultScreen!!) {
                            if (loginResponse?.isLiveness!! &&
                                Gson().fromJson(
                                    liveness?.get(context.getString(R.string.result)).toString(),
                                    com.perfios.objects.LivenessResult::class.java
                                ).livenessScore!! < loginResponse?.thresholdLiveness!!
                            ) {
                                startFaceMactchResultScreen(true)
                            } else {
                                startFaceMactchResultScreen(false)
                            }
                        } else {
                            getFaceMatchResult(null)
                        }
                    } else {
                        getFaceMatchResult(null)
                    }
                }
            }
        }
    }

    /**
     * TODO : call hyperverge facematch api.
     *
     * @param facematchresultCallBack : if intermediate screen present pass facematch  result call back instance or else pass null.
     */
    fun getFaceMatchResult(facematchresultCallBack: FaceMatchResultCallBack?) {

        HVNetworkHelper.makeFaceMatchCall(
            context, FaceMatchEndPoints.INDIA,
            faceImageURI,
            frontDocumentImageURI, jsonObjectParams, JSONObject()
        ) { hvError, result, _ ->
            //For Perfios
            finalPerfiosJsonObject.put(context.getString(R.string.perfios_face_match_data), result)

            //For app consumers
            faceMatchJsonObject.put(context.getString(R.string.perfios_face_match_data), result)

            if (hvError != null) {
                if (loginResponse?.retry == 0) {
                    InstancePool.kycResultCallBack?.onFailed(
                        hvError.errorMessage ?: context.getString(R.string.failed),
                        null
                    )
                }
                else
                {
                    if(hvError.errorMessage.contains(context.getString(R.string.host_error)) || hvError.errorMessage.contains("I/O"))
                    {
                        facematchresultCallBack?.onFailed(context.getString(R.string.no_network_error), null
                        )
                    }
                    else
                    {
                        facematchresultCallBack?.onFailed(
                            hvError.errorMessage ?: context.getString(R.string.failed), null
                        )
                    }
                }

            } else {

                if (facematchresultCallBack != null) {
                    val rawfaceResult = getFaceMatchResult(result)

                    val faceObject = Gson().fromJson(
                        rawfaceResult,
                        com.perfios.objects.FaceMatchResult::class.java
                    )

                    if (isFaceMatchConfidenceGood(
                            faceObject,
                            loginResponse?.thresholdFaceMatch!!
                        )
                    ) {
                        //sendFinalData(context, finalPerfiosJsonObject)
                        facematchresultCallBack.onSuccess(result, liveness)
                    } else {
                        facematchresultCallBack.onFailed(
                            context.getString(R.string.face_not_macthing),
                            null
                        )
                    }
                } else {
                    InstancePool.kycResultCallBack?.onSuccess(
                        clubbedResponse,
                        faceMatchJsonObject,
                        liveness,
                        faceImageURI
                    )
                    sendFinalData(context, finalPerfiosJsonObject)
                }
            }
        }
    }

    /**
     * TODO call liveness result screeen
     *
     * @param context  activity context
     * @param result result need to be shown in the liveness intermediate screen
     */
    private fun showLivenessResultScreen(
        context: Context,
        result: JSONObject
    ) {
        val intent = Intent(context, LivenessResult::class.java)
        intent.putExtra(context.getString(R.string.livenessResult), result.toString())
        context.startActivity(intent)
    }


    /**
     * @param context current activity context
     * @param docConfig customize document selection screen with the help of docConfig object
     * @param documentType select the type of the document selected For example ( AADHAR,PAN,PASSPORT,VOTERID)
     */
    fun startAppropriateDocumentActivity(
        context: Context,
        docConfig: HVDocConfig,
        documentType: DocumentType?
    ) {
        if (ocrEndPoint == null) {
            initOCREndPoints(documentType!!.name)
        }
        HVDocsActivity.start(context, docConfig) { error, result ->
            //make call to perfios database based on the document type
            if (error != null) {
                if(error.errorMessage.contains(context.getString(R.string.camera_permission_error)))
                {
                    InstancePool.kycResultCallBack?.onFailed(context.getString(R.string.camera_permission_denied), null)
                }else
                {
                    InstancePool.kycResultCallBack?.onFailed(error.errorMessage, null)
                }
                // TODO : Revisit this place again
                //val docType = DocType(documentType!!.name, "false", error.errorMessage)
//                Todo : uncomment below line to send selected document type to perfios
                //sendDocumentTypeToPerfios(docType)

            } else {
                documentImageURI = result.getString(context.getString(R.string.perfios_image_uri))
                // URI needed for face match
                if (docConfig.docCaptureSubText != context.getString(R.string.docCaptureBackSide)) {
                    frontDocumentImageURI =
                        result.getString(context.getString(R.string.perfios_image_uri))
                }

                if (documentType != null) {
                    this.documentType = documentType
                }

                this.docConfig = docConfig

                if (loginResponse?.isOCR!!) {

//                    frontDocumentImageURI = result.getString(context.getString(R.string.perfios_image_uri))

                    if (loginResponse?.retry!! > 0) {
                        startOcrResultScreen()
                    } else {
                        grabOCRData(null)
                        if (loginResponse?.isLiveness!! || loginResponse?.isFaceMatch!!) {
                            if (documentType!!.name != DocumentType.PAN.name && docConfig.docCaptureSubText != context.getString(
                                    R.string.docCaptureBackSide
                                )
                            ) {
                                captureDocumentBackside(context, documentType)
                            } else {
                                startFaceCapture()
                            }
                        } else if (loginResponse?.isFaceMatch == false && loginResponse?.isLiveness == false && docConfig.docCaptureSubText != context.getString(
                                R.string.docCaptureBackSide
                            )
                        ) {
                            captureDocumentBackside(context, documentType!!)

                        }
                    }

                } else if (loginResponse?.isLiveness!! || loginResponse?.isFaceMatch!!) {
                    startFaceCapture()
                } else {
                    InstancePool.kycResultCallBack?.onSuccess(
                        clubbedResponse,
                        result,
                        null,
                        faceImageURI
                    )
                }
            }
        }
    }

    /**
     * TODO Grab OCR data from the captured image.
     *
     * @param resultCallBack return the result callback.
     */
    fun grabOCRData(resultCallBack: ResultCallBack?) {
        val jsonObjectParams = JSONObject()
        // jsonObjectParams.put("enableDashboard", "yes")
        jsonObjectParams.put(
            context.getString(R.string.perfios_type),
            context.getString(R.string.perfios_id)
        )
        val headers = JSONObject()

        HVNetworkHelper.makeOCRCall(
            context, ocrEndPoint, documentImageURI, jsonObjectParams, headers
        ) { hvError, result, _ ->
            if (hvError != null) {
                if(hvError.errorMessage.contains(context.getString(R.string.host_error)))
                {
                    resultCallBack?.onFailed(context.getString(R.string.no_network_error), null)
                }
                else
                {
                    resultCallBack?.onFailed(hvError.errorMessage ?: "", null)
                }
                
                //  InstanceHelper.kycResultCallBack?.onFailed(hvError.errorMessage,null)
            } else {

                if (resultCallBack != null) {

                    val isConfidenceGood: Boolean =
                        isDocumentConfidenceGood(result, loginResponse?.thresholdOCR!!)

                    if (isConfidenceGood) {
                        commonConditionCheck(result, resultCallBack)
                    } else {
                        resultCallBack.onFailed("Document is not clear", null)
                    }

                } else {

                    commonConditionCheck(result, null)

                }
            }
        }
    }

    /**
     * TODO common conditions
     * @param result ocr result
     * @param resultCallBack ocr result callback
     */
    private fun commonConditionCheck(
        result: JSONObject,
        resultCallBack: ResultCallBack?
    ) {
        clubbedResponse.put(result)

        //Dedicated for single side capture
        if (documentType.name == DocumentType.PAN.name) {

            finalPerfiosJsonObject.put(
                context.getString(R.string.perfios_ocr_data),
                clubbedResponse
            )

            //document URI for facematch api call
            //frontDocumentImageURI =  result.getString(context.getString(R.string.perfios_image_uri))

            if (resultCallBack != null) {
                //TODO : send ocr result to intermediate screen
                resultCallBack.onSuccess(result)

                /*if(!loginResponse?.isLiveness!! && !loginResponse?.isFaceMatch!!)
                {

                    sendFinalData(context, finalPerfiosJsonObject)
                }*/
            } else if (!loginResponse?.isLiveness!! && !loginResponse?.isFaceMatch!!) {

                InstancePool.kycResultCallBack?.onSuccess(
                    clubbedResponse,
                    null,
                    null,
                    frontDocumentImageURI
                )

                sendFinalData(context, finalPerfiosJsonObject)
            }
        }
        //Dedicated for double side
        else if (documentType.name != DocumentType.PAN.name) {

            finalPerfiosJsonObject.put(
                context.getString(R.string.perfios_ocr_data),
                clubbedResponse
            )

            if (docConfig.docCaptureSubText == context.getString(R.string.docCaptureBackSide)) {

                if (resultCallBack != null) {
                    resultCallBack.onSuccess(result)

                    /* if(!loginResponse?.isLiveness!! && !loginResponse?.isFaceMatch!!)
                     {

                         sendFinalData(context, finalPerfiosJsonObject)
                     }*/
                } else if (!loginResponse?.isLiveness!! && !loginResponse?.isFaceMatch!!) {

                    InstancePool.kycResultCallBack?.onSuccess(
                        clubbedResponse,
                        null,
                        null,
                        frontDocumentImageURI
                    )

                    sendFinalData(context, finalPerfiosJsonObject)
                }

            } else
                resultCallBack?.onSuccess(result)

        }
    }

    /**
     * TODO start ocr result screen
     *
     */
    private fun startOcrResultScreen() {
        context.startActivity(Intent(context, OcrResult::class.java))
    }

    /**
     * TODO start facematch result screen
     *
     */
    private fun startFaceMactchResultScreen(livenessFailed: Boolean) {
        if (livenessFailed) {
            val intent = Intent(context, FaceMatchResult::class.java)
            intent.putExtra(context.getString(R.string.livenessFailed_key), livenessFailed)
            context.startActivity(intent)
        } else {
            context.startActivity(Intent(context, FaceMatchResult::class.java))
        }

    }


    /**
     * After completing document capture start the face capture.
     * Before making face capture send selected document to perfios.
     */
    fun startFaceCapture() {
        val faceConfig = HVFaceConfig()
        faceConfig.faceCaptureTitle = context.getString(R.string.faceCapture)
        faceConfig.isShouldShowInstructionPage = true
        if (loginResponse?.isLiveness!!) {
            faceConfig.setLivenessMode(HVFaceConfig.LivenessMode.TEXTURELIVENESS)
        }
        startFaceCaptureActivity(context, faceConfig)
    }

    /**
     * Send document OCR data to perfios.
     *  @param clubbedData : Document front and back OCR data.
     */
    fun sendFinalData(context: Context, clubbedData: JSONObject) {
        Log.d("PerfiosSDK-Monitor", "Inside SendFinalData")
        if (InstancePool.location != null) {
            clubbedData.put(context.getString(R.string.location), InstancePool.location)
        }
        val requestBody: RequestBody = RequestBody.create(
            okhttp3.MediaType.parse(
                context.getString(
                    R.string.perfios_request_body_type
                )
            ), clubbedData.toString()
        )
        val call = perfioService.sendFinalData(
            requestBody,
            InstancePool.organization!!,
            InstancePool.perfiosTransactionID!!
        )
        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {

                //Todo: currently no logic required inside this block. May it help  future developer.

            }

            override fun onFailure(call: Call<Void>, t: Throwable) {
                //kycResultCallBack?.onFailed(t.message.toString(), t)
                //Todo: currently no logic required inside this block. May it help  future developer.
            }
        })
    }

    fun recaptureDocument() {
        startAppropriateDocumentActivity(
            context,
            getDocConfig(context, documentType, false),
            documentType
        )
    }


    /**
     * Methods helps to capture the document backside
     * @param context : client app context
     * @param documentType : document selected by the user. Eg :  AADHAR,PAN,PASSPORT,VOTERID
    }
     */
    fun captureDocumentBackside(context: Context, documentType: DocumentType) {
        startAppropriateDocumentActivity(
            context,
            getDocConfig(context, documentType, true),
            documentType
        )
    }


    /**
     * initialize OCR end points based on the document type.
     * @param docName : name of the document to be captured.
     */
    private fun initOCREndPoints(docName: String) {
        when (docName) {
            DocumentType.AADHAR.name -> {
                ocrEndPoint = OcrIndiaEndPoints.AADHAR

            }
            DocumentType.PAN.name -> {
                ocrEndPoint = OcrIndiaEndPoints.PAN

            }
            DocumentType.PASSPORT.name -> {
                ocrEndPoint = OcrIndiaEndPoints.PASSPORT

            }
            DocumentType.VOTERID.name -> {
                ocrEndPoint = OcrIndiaEndPoints.KYC

            }
        }
    }

    /**
     * First method to be called to start perfios transaction.
     * @param context : client app context.
     * @param loginRequest : login request object.
     * @param kycResultCallBack : call back reference to send the OCR ,faceMatch and error to client app.
     * @param documentType : document type needs to be captured For ex : Eg :  AADHAR,PAN,PASSPORT,VOTERID.
     * @param org : organization name registered with perfios.
     */
    fun makeLogin(
        context: Context,
        loginRequest: LoginRequest,
        kycResultCallBack: KYCResultCallBack?,
        documentType: DocumentType?, org: String
    ) {
        InstancePool.organization = org
        this.context = context
        if (kycResultCallBack != null && kycResultCallBack != InstancePool.kycResultCallBack) {
            InstancePool.kycResultCallBack = kycResultCallBack
        }
        val call = perfioService.login(loginRequest, org)
        call.enqueue(object : Callback<LoginResponse> {
                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                if (response.isSuccessful) {

                    loginResponse = response.body()
                    //loginResponse?.showSelfieResultScreen = false

                    //Make sure duplicate location is not sent.
                    InstancePool.location = null
                    InstancePool.retryCounter = 1

                    //TODO : Only for development purpose
                    /*loginResponse?.isLiveness = false
                     loginResponse?.isFaceMatch = false
                     loginResponse?.isOCR = true
                    loginResponse?.thresholdOCR =30
                   loginResponse?.thresholdLiveness =0.1f
                   loginResponse?.thresholdFaceMatch =30
                   loginResponse?.geoTagging = 1
                   loginResponse?.retry = 2
                    loginResponse?.showSelfieResultScreen = false
                    loginResponse?.isLiveness = true
                    loginResponse?.isFaceMatch = true
                    loginResponse?.isOCR = true*/

                    if (loginResponse?.geoTagging!! > 0) {

                        if (loginResponse?.geoTagging!! == 1) {
                            context.startActivity(Intent(context, PermissionActivity::class.java))
                        } else {
                            val intent = Intent(context, PermissionActivity::class.java)
                            intent.putExtra(
                                context.getString(R.string.should_show), context.getString(
                                    R.string.yes
                                )
                            )
                            context.startActivity(intent)
                        }

                        InstancePool.locationCallBack = object : LocationCallBack {
                            override fun isSuccess(isSuccess: Boolean) {
                                if (isSuccess) {
                                    GotoHyperverge(loginResponse, documentType)
                                } else {
                                    kycResultCallBack?.onFailed(
                                        "Location denied by the user.",
                                        null
                                    )
                                }
                            }

                        }
                    } else {
                        GotoHyperverge(loginResponse, documentType)
                    }
                } else {
                    var apiError : APIError? = ErrorUtils.parseError(response)
                    kycResultCallBack?.onFailed(
                        apiError?.message?:context.getString(R.string.perfios_login_failed),
                        null
                    )
                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                if(t is IOException)
                {
                    kycResultCallBack?.onFailed(context.getString(R.string.no_network_error), t)
                }
                else
                {
                    kycResultCallBack?.onFailed(t.message.toString(), t)
                }

            }
        })
    }

    private fun GotoHyperverge(
        loginResponse: LoginResponse?,
        documentType: DocumentType?
    ) {
        if (loginResponse != null) {

            InstancePool.perfiosTransactionID = loginResponse.perfiosTransactionId

            //Initialize PerfiosSDK
            init(
                context,
                decodeData(loginResponse.appID ?: ""),
                decodeData(loginResponse.appKey ?: ""),
                getLocale(context)
            )

            //if Document type is null it means user want to utilize the SDK provided document selection screen*/
            when (documentType) {
                null -> {
                    if (loginResponse.isOCR || loginResponse.isFaceMatch) {
                        context.startActivity(
                            Intent(
                                context,
                                SelectDocument::class.java
                            )
                        )
                    } else if (!loginResponse.isOCR && (loginResponse.isFaceMatch || loginResponse.isLiveness)) {
                        startFaceCapture()
                    }
                    else
                    {
                        InstancePool.kycResultCallBack?.onFailed("No transaction enabled. Please contact perfios support team. ", null)
                    }

                }
                else -> {
                    if (loginResponse.isOCR || loginResponse.isFaceMatch) {
                        sendDocumentTypeToPerfios(
                            DocType(
                                documentType.name,
                                context.getString(R.string.perfios_true),
                                ""
                            )
                        )

                        startAppropriateDocumentActivity(
                            context,
                            getDocConfig(context, documentType, false), documentType
                        )
                    } else if (!loginResponse.isOCR && (loginResponse.isFaceMatch || loginResponse.isLiveness)) {
                        startFaceCapture()
                    }
                    else
                    {
                        InstancePool.kycResultCallBack?.onFailed("No transaction enabled. Please contact perfios support team. ", null)
                    }

                }
            }
        }
    }


    /**
     * send selected document type to perfios.
     * @param docType : document type needs to be captured For ex:  AADHAR,PAN,PASSPORT,VOTERID.
     */
    fun sendDocumentTypeToPerfios(docType: DocType) {
        Log.d("PerfiosSDK-Monitor", "Inside sendDocumentTypeToPerfios ")
        val call =
            perfioService.sendDoctypeToPerfios(
                docType,
                InstancePool.organization!!,
                InstancePool.perfiosTransactionID ?: ""
            )
        call.enqueue(object : Callback<Void> {
            override fun onResponse(call: Call<Void>, response: Response<Void>) {
                if (response.isSuccessful) {
                    //Todo: currently no logic required inside this block. May it help  future developer.

                }
            }

            override fun onFailure(call: Call<Void>, t: Throwable) {

                //Todo: currently no logic required inside this block. May it help  future developer.

            }
        })
    }


}

