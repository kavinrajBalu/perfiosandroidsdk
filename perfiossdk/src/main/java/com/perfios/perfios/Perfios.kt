package com.perfios.perfios

import android.content.Context
import com.perfios.interfaces.KYCResultCallBack
import com.perfios.objects.DocumentType
import com.perfios.objects.InstancePool
import com.perfios.objects.LoginRequest
import com.perfios.utils.encodeData

/**
 * Using this class start the KYC transaction. If initialization failed then you will be not allowed to do KYC.
 */
 object Perfios{
    /**
     * TODO : If you want to use the library document selection screen then use this method.
     * @param context current activity context.
     * @param clientID client ID provided by perfios.
     * @param secret   your secret password set from perfios system.
     * @param organization your organization name registered with perfios.
     */
    fun init(context : Context, clientID:String, secret:String, organization: String, kycResultCallBack: KYCResultCallBack?) {
        //step 1 : Build loginRequest object.
        val loginRequest = LoginRequest(encodeData(clientID), encodeData(secret))

        val perfiosSDK = PerfiosSDK()

        InstancePool.perfiosSDK = perfiosSDK

        //step 2 : Make login call to perfios and take user to document selection screen.
        InstancePool.perfiosSDK!!.makeLogin(context,loginRequest,kycResultCallBack,null,organization)
    }

    /**
     * TODO : If you know what document need to be captured use this method.
     * @param context current activity context.
     * @param clientID client ID provided by perfios.
     * @param secret   your secret password set from perfios system.
     * @param organization your organization name registered with perfios.
     * @param documentType type of document need to be verified. Ex: AADHAR,PAN,PASSPORT,VOTERID
     */
    fun init(context : Context, clientID:String, secret:String, organization: String, documentType: DocumentType, kycResultCallBack: KYCResultCallBack) {
        //step 1 : Build loginRequest object.
        val loginRequest = LoginRequest(encodeData(clientID), encodeData(secret))

        val perfiosSDK = PerfiosSDK()

        InstancePool.perfiosSDK = perfiosSDK


        //step 2 : Make login call to perfios and take to HV document capture screen.
        InstancePool.perfiosSDK!!.makeLogin(context,loginRequest,kycResultCallBack,documentType,organization)
    }
}