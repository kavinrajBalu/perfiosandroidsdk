package com.perfios.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.perfios.card.objects.*
import com.perfios.interfaces.ResultCallBack
import com.perfios.objects.DocumentTypeResponseNames
import com.perfios.objects.InstancePool
import com.perfios.utils.getCardResultJSON
import com.perfios.utils.getExactCardType
import org.json.JSONObject

/**
 * TODO OCR intermediate view model class
 *
 */
 internal class OcrResultViewModel :ViewModel(){

    var  ocrError = MutableLiveData<String>()

    var resultMap = MutableLiveData<LinkedHashMap<String,String>>()

    init {
        Log.i("OcrResultViewModel","OcrResultViewModel created!")
        makeOcrCall()
    }

    private fun makeOcrCall() {
        InstancePool.perfiosSDK?.grabOCRData(object : ResultCallBack {
            override fun onSuccess(ocrResult: JSONObject) {
                val cardType = getExactCardType(ocrResult)

                val jsonCardData = getCardResultJSON(ocrResult)

                resultMap.value = getCardValueInHashMap(jsonCardData,cardType)!!

            }
            override fun onFailed(_error: String, t: Throwable?) {
                ocrError.value = _error
            }
        })
    }

    /**
     * TODO : Method used to parse JSON object to custom object class.
     *
     * @param result : JSON object.
     * @param cardType : Card type received from response.
     * @return  return hashmap value .
     */
    fun getCardValueInHashMap(result: String, cardType:String):LinkedHashMap<String,String>?
    {

        when (cardType) {
            DocumentTypeResponseNames.pan.toString() ->{

                val pan = Gson().fromJson(result, Pan::class.java)

                return getMapValueforPan(pan)


            }

            DocumentTypeResponseNames.old_pan.toString() -> {
                val pan = Gson().fromJson(result, OldPan::class.java)
                return getMapValueforOldPan(pan)
            }

            DocumentTypeResponseNames.aadhaar_front_bottom.toString() -> {

                val aadhar =Gson().fromJson(result, AadhaarFrontBottom::class.java)
                return getMapValueForAadharFrontBottom(aadhar)
            }

            DocumentTypeResponseNames.aadhaar_back.toString() -> {

                val aadhar = Gson().fromJson(result, AadhaarBack::class.java)
                return getMapValueForAadharBack(aadhar)
            }

              DocumentTypeResponseNames.aadhaar_front_top.toString() -> {

                  val aadhar =  Gson().fromJson(result, AadhaarFrontTop::class.java)
                  return  getMapValueForAadharFrontTop(aadhar)
              }

             DocumentTypeResponseNames.aadhaar_back.toString() -> {

                 val aadhar = Gson().fromJson(result, AadhaarBack::class.java)
                 return getMapValueForAadharBack(aadhar)
             }

             DocumentTypeResponseNames.passport_front.toString() -> {
                 val passport = Gson().fromJson(result, PassportFront::class.java)
                 return getMapValueForPassportFront(passport)
             }

             DocumentTypeResponseNames.passport_back.toString() -> {
                 val passport = Gson().fromJson(result, PassportBack::class.java)
                 return getMapValueForPassportBack(passport)
             }

             DocumentTypeResponseNames.voterid_front.toString() -> {

                 val voterId = Gson().fromJson(result, VoterIdFront::class.java)
                 return getMapValueForVoterIdFront(voterId)
             }

             DocumentTypeResponseNames.voterid_front_new.toString() -> {
                 val voterId = Gson().fromJson(result, VoterIdFrontNew::class.java)
                 return getMapValueForVoterIdFrontNew(voterId)
             }

             DocumentTypeResponseNames.voterid_back.toString() -> {
                 val voterId = Gson().fromJson(result, VoterIdBack::class.java)
                 return getMapValueForVoterIdBack(voterId)
             }

 else -> {
     return null
 }
}
}

    /**
     * TODO Retrieve
     *
     * @param aadhaarFrontBottom object
     * @return hashmap
     */

    private fun getMapValueForAadharFrontBottom(aadhaarFrontBottom: AadhaarFrontBottom?): java.util.LinkedHashMap<String,String>? {

        val aadharFrontBottomMap = LinkedHashMap<String,String>()
        aadharFrontBottomMap["Name"] = aadhaarFrontBottom?.name?.value.toString()
        aadharFrontBottomMap["Aadhar"] = aadhaarFrontBottom?.aadhar?.value.toString()
        aadharFrontBottomMap["Gender"] = aadhaarFrontBottom?.gender?.value.toString()
        aadharFrontBottomMap["Dob"] = aadhaarFrontBottom?.dob?.value.toString()
        aadharFrontBottomMap["Yob"] = aadhaarFrontBottom?.yob?.value.toString()
        aadharFrontBottomMap["Father"] = aadhaarFrontBottom?.father?.value.toString()
        aadharFrontBottomMap["Mother"] = aadhaarFrontBottom?.mother?.value.toString()
        return aadharFrontBottomMap
    }


    /**
     * TODO Retrieve
     *
     * @param pan object
     * @return hashmap
     */
    private fun getMapValueforPan(pan: Pan?): LinkedHashMap<String,String> {
        val panMap = LinkedHashMap<String,String>()
        panMap["Pan Number"] = pan?.panNumber?.value.toString()
        panMap["Name"] = pan?.name?.value.toString()
        panMap["Date of Issue"] = pan?.dateOfIssue?.value.toString()
        panMap["Date"] = pan?.date?.value.toString()
        panMap["Father"] = pan?.father?.value.toString()
        return panMap
}


    /**
     * TODO Retrieve
     *
     * @param pan old object
     * @return hashmap
     */
    private fun getMapValueforOldPan(pan: OldPan?): LinkedHashMap<String,String> {
        val panMap = LinkedHashMap<String,String>()
        panMap["Pan Number"] = pan?.panNumber?.value.toString()
        panMap["Name"] = pan?.name?.value.toString()
        panMap["Date"] = pan?.date?.value.toString()
        panMap["Father"] = pan?.father?.value.toString()
        return panMap
    }


    /**
     * TODO Retrieve
     *
     * @param aadhaarBack object
     * @return hashmap
     */
    private fun getMapValueForAadharBack(aadhaarBack: AadhaarBack?): java.util.LinkedHashMap<String,String>? {

        val aadharBack = LinkedHashMap<String,String>()
        aadharBack["Aadhar"] = aadhaarBack?.aadhaar?.value.toString()
        aadharBack["Address"] = aadhaarBack?.address?.value.toString()
        aadharBack["Father"] = aadhaarBack?.father?.value.toString()
        aadharBack["Husband"] = aadhaarBack?.husband?.value.toString()
        aadharBack["Pin"] = aadhaarBack?.pin?.value.toString()
        return aadharBack
    }


    /**
     * TODO Retrieve
     *
     * @param aadhaarFrontTop object
     * @return hashmap
     */
    private fun getMapValueForAadharFrontTop(aadhaarFrontTop: AadhaarFrontTop?): java.util.LinkedHashMap<String,String>? {

        val aadharfrontTop = LinkedHashMap<String,String>()
        aadharfrontTop["Name"] = aadhaarFrontTop?.name?.value.toString()
        aadharfrontTop["Aadhar"] = aadhaarFrontTop?.aadhaar?.value.toString()
        aadharfrontTop["Father"] = aadhaarFrontTop?.father?.value.toString()
        aadharfrontTop["Husband"] = aadhaarFrontTop?.husband?.value.toString()
        aadharfrontTop["Address"] = aadhaarFrontTop?.address?.value.toString()
        aadharfrontTop["Pin"] = aadhaarFrontTop?.pin?.value.toString()
        return aadharfrontTop
    }

    /**
     * TODO Retrieve
     *
     * @param passportFront object
     * @return hashmap
     */
    private fun getMapValueForPassportFront(passportFront: PassportFront?): java.util.LinkedHashMap<String,String>? {

        val passportFrontTop  = LinkedHashMap<String,String>()
        passportFrontTop["Passport number"] = passportFront?.passportNumber?.value.toString()
        passportFrontTop["Name"] = passportFront?.givenName?.value.toString()
        passportFrontTop["Surname"] = passportFront?.surname?.value.toString()
        passportFrontTop["Dob"] = passportFront?.dob?.value.toString()
        passportFrontTop["Country code"] = passportFront?.countryCode?.value.toString()
        passportFrontTop["Doe"] = passportFront?.doe?.value.toString()
        passportFrontTop["Nationality"] = passportFront?.nationality?.value.toString()
        passportFrontTop["Place of birth"] = passportFront?.placeOfBirth?.value.toString()
        passportFrontTop["Place of issue"] = passportFront?.placeOfIssue?.value.toString()
        passportFrontTop["Type"] = passportFront?.type?.value.toString()
        return passportFrontTop
    }

    /**
     * TODO Retrieve
     *
     * @param passportBack object
     * @return hashmap
     */
    private fun getMapValueForPassportBack(passportBack: PassportBack?): java.util.LinkedHashMap<String,String>? {

        val passportBackMap  = LinkedHashMap<String,String>()
        passportBackMap["Passport number"] = passportBack?.passportNumber?.value.toString()
        passportBackMap["Address"] = passportBack?.address?.value.toString()
        passportBackMap["Father"] = passportBack?.father?.value.toString()
        passportBackMap["File number"] = passportBack?.fileNumber?.value.toString()
        passportBackMap["Mother"] = passportBack?.mother?.value.toString()
        passportBackMap["Old doi"] = passportBack?.oldDoi?.value.toString()
        passportBackMap["Old passport number"] = passportBack?.oldPassportNumber?.value.toString()
        passportBackMap["Old place of issue"] = passportBack?.oldPlaceOfIssue?.value.toString()
        passportBackMap["Pin"] = passportBack?.pin?.value.toString()
        passportBackMap["Spouse"] = passportBack?.spouse?.value.toString()
        return passportBackMap
    }


    /**
     * TODO Retrieve
     *
     * @param voterIdFront object
     * @return hashmap
     */
    private fun getMapValueForVoterIdFront(voterIdFront: VoterIdFront?): java.util.LinkedHashMap<String,String>? {

        val voterIDFrontMap  = LinkedHashMap<String,String>()
        voterIDFrontMap["Name"] = voterIdFront?.name?.value.toString()
        voterIDFrontMap["Voter id"] = voterIdFront?.voterid?.value.toString()
        voterIDFrontMap["Dob"] = voterIdFront?.dob?.value.toString()
        voterIDFrontMap["Gender"] = voterIdFront?.gender?.value.toString()
        voterIDFrontMap["Doc"] = voterIdFront?.doc?.value.toString()
        voterIDFrontMap["Relation"] = voterIdFront?.relation?.value.toString()
        voterIDFrontMap["Age"] = voterIdFront?.age?.value.toString()
        return voterIDFrontMap
    }


    /**
     * TODO Retrieve
     *
     * @param voterIdFrontNew object
     * @return hashmap
     */
    private fun getMapValueForVoterIdFrontNew(voterIdFrontNew: VoterIdFrontNew?): java.util.LinkedHashMap<String,String>? {

        val voterIdFrontNewMap  = LinkedHashMap<String,String>()
        voterIdFrontNewMap["Name"] = voterIdFrontNew?.name?.value.toString()
        voterIdFrontNewMap["Voter id"] = voterIdFrontNew?.voterid?.value.toString()
        voterIdFrontNewMap["Relation"] = voterIdFrontNew?.relation?.value.toString()
        return voterIdFrontNewMap
    }


    /**
     * TODO Retrieve
     *
     * @param voterIdFront object
     * @return hashmap
     */
    private fun getMapValueForVoterIdBack(voterIdFront: VoterIdBack?): java.util.LinkedHashMap<String,String>? {

        val voterIDBackMap  = LinkedHashMap<String,String>()
        voterIDBackMap["Voter id"] = voterIdFront?.voterid?.value.toString()
        voterIDBackMap["Pin"] = voterIdFront?.pin?.value.toString()
        voterIDBackMap["Address"] = voterIdFront?.address?.value.toString()
        voterIDBackMap["Type"] = voterIdFront?.type?.value.toString()
        voterIDBackMap["Dob"] = voterIdFront?.dob?.value.toString()
        voterIDBackMap["Gender"] = voterIdFront?.gender?.value.toString()
        voterIDBackMap["Age"] = voterIdFront?.age?.value.toString()
        return voterIDBackMap
    }





}
