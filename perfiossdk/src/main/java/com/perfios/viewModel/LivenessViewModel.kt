package com.perfios.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.perfios.objects.LivenessResult

/**
 * TODO liveness result viewmodel
 *
 * @constructor
 * TODO : view model is built using viewmodel factory
 *
 * @param livenessResult liveness result
 */
  internal class LivenessViewModel(livenessResult:LivenessResult)  : ViewModel()
{
    var livenessResultLiveData = MutableLiveData<LinkedHashMap<String,String>>()

    init {
            livenessResultLiveData.value = getLivenessMap(livenessResult)
    }

    private fun getLivenessMap(livenessResult: LivenessResult): java.util.LinkedHashMap<String, String>? {
        val livenessResultMap = LinkedHashMap<String,String>()
        livenessResultMap["Live"] = livenessResult.live?:""
        livenessResultMap["Liveness Score"] = livenessResult.livenessScore.toString()
        livenessResultMap["To Be Reviewed"] = livenessResult.toBeReviewed?:""
        return  livenessResultMap
    }
}