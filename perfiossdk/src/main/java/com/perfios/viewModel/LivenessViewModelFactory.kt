package com.perfios.viewModel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.perfios.objects.LivenessResult
import com.perfios.perfiossdk.R

  internal class LivenessViewModelFactory(
    private val livenessResult: LivenessResult,
    private val context: Context
) : ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if(modelClass.isAssignableFrom(LivenessViewModel::class.java))
        {
            return LivenessViewModel(livenessResult) as T
        }

        throw  IllegalArgumentException(context.getString(R.string.error_message))
    }


}