package com.perfios.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.perfios.interfaces.FaceMatchResultCallBack
import com.perfios.objects.FaceMatchResult
import com.perfios.objects.InstancePool
import com.perfios.objects.LivenessResult
import com.perfios.utils.getFaceMatchResult
import org.json.JSONObject

/**
 * TODO Face match result view model
 *
 */
  internal class FaceMatchResultViewModel : ViewModel()
{
    var  faceMatchError = MutableLiveData<String>()

    var resultMap = MutableLiveData<LinkedHashMap<String,String>>()

    var livenessResult:LivenessResult? = null


    init {
       callFaceMatchCall()
    }

    private fun callFaceMatchCall() {
            val livenessResultObject = Gson().fromJson(InstancePool.perfiosSDK?.liveness?.get("result").toString(), LivenessResult::class.java)

            if(livenessResultObject!=null && livenessResultObject.live!="yes")
            {
                faceMatchError.value = "Photo is not live"
            }
            else
            {
                InstancePool.perfiosSDK?.getFaceMatchResult(object : FaceMatchResultCallBack {
                    override fun onSuccess(faceResult: JSONObject, liveness: JSONObject?) {

                        if(InstancePool.perfiosSDK?.loginResponse?.isLiveness == true && liveness!=null)
                        {
                             livenessResult = Gson().fromJson(liveness.get("result").toString(),LivenessResult::class.java)
                        }

                        val rawfaceResult = getFaceMatchResult(faceResult)

                        val faceObject  =  Gson().fromJson(rawfaceResult, FaceMatchResult::class.java)

                        resultMap.value = getFaceMatchMap(faceObject,livenessResult)

                    }

                    override fun onFailed(error: String, t: Throwable?) {
                        faceMatchError.value  = error
                    }
                })

            }

    }

    private fun getFaceMatchMap(
        faceObject: FaceMatchResult?,
        livenessResult: LivenessResult?
    ): java.util.LinkedHashMap<String, String>? {

        val faceMatchMap = LinkedHashMap<String,String>()
        faceMatchMap["Match"] = faceObject?.match.toString()
        faceMatchMap["Match Score"] = faceObject?.matchScore.toString()
        faceMatchMap["To Be Reviewed"] = faceObject?.toBeReviewed.toString()

        if(livenessResult!=null)
        {
            faceMatchMap["Live"] = livenessResult.live?:""
            faceMatchMap["Liveness Score"] = livenessResult.livenessScore.toString()
            faceMatchMap["To Be Reviewed"] = livenessResult.toBeReviewed?:""
        }
        return  faceMatchMap
    }
}