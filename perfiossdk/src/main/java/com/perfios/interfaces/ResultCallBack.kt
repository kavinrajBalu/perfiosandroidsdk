package com.perfios.interfaces

import org.json.JSONObject

/**
 * TODO : Call back used for intermediate screens.
 *
 */
  internal interface ResultCallBack {
    /**
     * @param result :ocr result result from the each
     */
    fun onSuccess(result: JSONObject)

    /**
     * @param error  : transaction error message
     * @param t      : throwable errors
     */
    fun onFailed(error:String,t : Throwable?)
}