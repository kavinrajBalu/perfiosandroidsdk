package com.perfios.interfaces

import org.json.JSONObject

  internal interface FaceMatchResultCallBack {

    /**
     * @param faceResult : faceResult extracted data from face capture.
     */
    fun onSuccess(faceResult: JSONObject , liveness:JSONObject?)

    /**
     * @param error  : transaction error message
     * @param t      : throwable errors
     */
    fun onFailed(error:String,t : Throwable?)
}