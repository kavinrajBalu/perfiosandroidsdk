package com.perfios.interfaces

/**
 * TODO Location call back helps to checks user enabled/disabled device location.
 *
 */
  internal interface LocationCallBack {

    /**
     * @param isSuccess : if location is success then return true else false.
     */
    fun isSuccess(isSuccess:Boolean)
}