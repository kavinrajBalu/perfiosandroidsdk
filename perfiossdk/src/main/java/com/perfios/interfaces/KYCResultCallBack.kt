package com.perfios.interfaces

import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable

/**
 * KYC result call back interface.
 *
 */
interface KYCResultCallBack : Serializable {
    /**
     * @param ocrResult : OCR extracted data from document capture.
     * @param faceMatchResult  : face match result in JSON object format.
     * @param imageURI : document image URI received from document capture.
     */
    fun onSuccess(ocrResult: JSONArray?, faceMatchResult:JSONObject?, livenessResult: JSONObject?, imageURI:String)

    /**
     * @param error  : transaction error message
     * @param t      : throwable errors
     */
    fun onFailed(error:String,t : Throwable?)
}