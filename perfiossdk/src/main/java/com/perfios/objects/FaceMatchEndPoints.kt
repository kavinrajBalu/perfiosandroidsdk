package com.perfios.objects

/**
 * Face match endpoints based on the region
 */
  internal object FaceMatchEndPoints {
    const val INDIA = "https://ind-faceid.hyperverge.co/v1/photo/verifyPair"
    const val ASIA_PACIFIC = "https://apac.faceid.hyperverge.co/v1/photo/verifyPair"
}