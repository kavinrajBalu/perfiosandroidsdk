package com.perfios.objects

import com.google.gson.annotations.SerializedName

/**
* PerfiosTransaction ID is used in API 2 and API 3 to update document type and scanned document data into com.perfios.perfios.
* AppId and AppKey is used to initialize hyperverge.
*/
  internal class LoginResponse {
    @SerializedName("clientTransactionId")
    var clientTransactionId: String? = null

    @SerializedName("perfiosTransactionId")
    var perfiosTransactionId: String? = null

    @SerializedName("appID")
    var appID:String? =null

    @SerializedName("appKey")
    var appKey: String? = null

    @SerializedName("ocr")
    var isOCR : Boolean = true

    @SerializedName("facematch")
    var isFaceMatch : Boolean = true

    @SerializedName("liveness")
    var isLiveness : Boolean = true

    @SerializedName("threshold_ocr")
    var thresholdOCR : Int = 10

    @SerializedName("threshold_facematch")
    var thresholdFaceMatch : Int = 80

    @SerializedName("threshold_liveness")
    var thresholdLiveness : Float = 0.0f

    @SerializedName("retry")
    var retry : Int = 1

    @SerializedName("geotagging")
    var geoTagging : Int? = 2

    @SerializedName("showSelfieResultScreen")
    var showSelfieResultScreen : Boolean =  false
}