package com.perfios.objects

/**
 * TODO KYC document type names
 *
 */
  enum class DocumentTypeResponseNames {

    pan,
    old_pan,
    aadhaar_front_bottom,
    aadhaar_front_top,
    aadhaar_back,
    passport_front,
    passport_back,
    voterid_front,
    voterid_front_new,
    voterid_back
}