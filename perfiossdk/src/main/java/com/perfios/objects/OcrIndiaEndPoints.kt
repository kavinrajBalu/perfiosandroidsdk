package com.perfios.objects

/**
 * End points for OCR API call.
 */
   internal object OcrIndiaEndPoints {
    const val PAN = "https://ind-docs.hyperverge.co/v2.0/readPAN"
    const val AADHAR = "https://ind-docs.hyperverge.co/v2.0/readAadhaar"
    const val PASSPORT = "https://ind-docs.hyperverge.co/v2.0/readPassport"
    const val KYC = "https://ind-docs.hyperverge.co/v2.0/readKYC"
}