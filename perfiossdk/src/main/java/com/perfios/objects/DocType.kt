package com.perfios.objects

/**
* This object is used to send selected document type  to com.perfios.perfios.
 * @param docType selected document type Eg.(AADHAR,PAN,PASSPORT,VOTERID)
 * @param success : If document capture result success then pass true else false.
 * @param timestamp : Current device timestamp. Format : dd-MM-yyyy HH:mm:ss z
* */

  internal data class DocType(var docType: String,var success: String,var description: String,
                            var timestamp: String = com.perfios.utils.getCurrentDateTime() )