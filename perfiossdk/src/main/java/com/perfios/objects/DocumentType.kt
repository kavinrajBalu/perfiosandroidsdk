package com.perfios.objects

/**
 * Supported document types
 *
 */
enum class DocumentType
{
    AADHAR,
    PAN,
    PASSPORT,
    VOTERID
}