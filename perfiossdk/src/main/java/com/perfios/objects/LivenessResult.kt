package com.perfios.objects

import com.google.gson.annotations.SerializedName

/**
 * TODO Liveness result based on the selfie capture
 *
 * @property live boolean type
 * @property livenessScore liveness score
 * @property toBeReviewed boolean type
 */
 data class LivenessResult(

    @SerializedName("live")
    var live: String? = null,

    @SerializedName("liveness-score")
    var livenessScore: Float? = null,

    @SerializedName("to-be-reviewed")
    var toBeReviewed: String? = null
)