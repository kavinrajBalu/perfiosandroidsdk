package com.perfios.objects

import com.google.gson.annotations.SerializedName

/**
 * TODO : Face Match result
 *
 * @property conf confidence value based on selfie and document front capture
 * @property match match boolean
 * @property matchScore match score
 * @property toBeReviewed  to be reviewed boolean type
 */
 data class FaceMatchResult(
    @SerializedName("conf")
    var conf: Int? = null,

    @SerializedName("match")
    var match: String? = null,

    @SerializedName("match-score")
    var matchScore: Int? = null,

    @SerializedName("to-be-reviewed")
    var toBeReviewed: String? = null
)