package com.perfios.objects
/**
 * TODO
 *
 * @property clientId clientID received from perfios
 * @property secret secret received from perfios
 * @property clientTransactionId client transaction used to monitor KYC.
 * @property timestamp  current system timestamp
 */
  internal data class LoginRequest(val clientId:String,
                                 val secret:String,
                                 var clientTransactionId:String = com.perfios.utils.getUniqueTransactionID(),
                                 var timestamp:String = com.perfios.utils.getCurrentDateTime())
{
    var deviceID:String = ""
}