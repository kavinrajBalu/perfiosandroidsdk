package com.perfios.objects

import com.perfios.interfaces.KYCResultCallBack
import com.perfios.interfaces.LocationCallBack
import com.perfios.perfios.PerfiosSDK

/**
 * Instance helper class
 */
  internal object InstancePool {
    var kycResultCallBack : KYCResultCallBack? = null
    var organization: String? = null
    var perfiosTransactionID: String? = null
    var perfiosSDK : PerfiosSDK? = null
    var location: String? = null
    var locationCallBack : LocationCallBack? = null
    var retryCounter : Int = 1
}