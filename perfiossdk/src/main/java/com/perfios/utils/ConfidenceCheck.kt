package com.perfios.utils

import com.google.gson.Gson
import com.perfios.card.objects.*
import com.perfios.objects.FaceMatchResult
import org.json.JSONObject

/**
 * TODO
 *
 * @param result : OCR result.
 * @param threshold : threshold received from the response.
 * @return
 */
  internal fun isDocumentConfidenceGood(result: JSONObject?, threshold: Int): Boolean {

    //Card Types
    val pan = "pan"
    val oldPan = "old_pan"
    val aadhaarFrontBottom = "aadhaar_front_bottom"
    val aadhaarFrontTop = "aadhaar_front_top"
    val aadhaarBack = "aadhaar_back"
    val passportFront = "passport_front"
    val passportBack = "passport_back"
    val voterIdFront = "voterid_front"
    val voterIdFrontNew = "voterid_front_new"
    val voterIdBack = "voterid_back"

    //Card type from response
    val cardType = getExactCardType(result)

    //result from the JSON
    val resultJSON: String =
        (result?.getJSONArray("result")?.get(0) as JSONObject).getJSONObject("details").toString()

    //return the result based on the confidence level
    when (cardType) {
        pan -> return checkPanConfidence(Gson().fromJson(resultJSON, Pan::class.java), threshold)

        oldPan -> return checkOldPanConfidence(
            Gson().fromJson(resultJSON, OldPan::class.java),
            threshold
        )

        aadhaarFrontBottom -> return checkAadhaarFrontBottomConfidence(
            Gson().fromJson(
                resultJSON,
                AadhaarFrontBottom::class.java
            ), threshold
        )

        aadhaarFrontTop -> return checkAadhaarFrontTopConfidence(
            Gson().fromJson(
                resultJSON,
                AadhaarFrontTop::class.java
            ), threshold
        )

        aadhaarBack -> return checkAadhaarBackConfidence(
            Gson().fromJson(
                resultJSON,
                AadhaarBack::class.java
            ), threshold
        )

        passportFront -> return checkPassportFrontConfidence(
            Gson().fromJson(
                resultJSON,
                PassportFront::class.java
            ), threshold
        )

        passportBack -> return checkPassportBackConfidence(
            Gson().fromJson(
                resultJSON,
                PassportBack::class.java
            ), threshold
        )

        voterIdFront -> return checkVoterIdFrontConfidence(
            Gson().fromJson(
                resultJSON,
                VoterIdFront::class.java
            ), threshold
        )

        voterIdFrontNew -> return checkVoterIdFrontNewConfidence(
            Gson().fromJson(
                resultJSON,
                VoterIdFrontNew::class.java
            ), threshold
        )

        voterIdBack -> return checkVoterIdBackConfidence(
            Gson().fromJson(
                resultJSON,
                VoterIdBack::class.java
            ), threshold
        )

        else -> {
            return false
        }
    }


}

/**
 * TODO :
 *
 * @param voterIdBack voterIDBack object
 * @param threshold confidence threshold
 * @return
 */
  internal fun checkVoterIdBackConfidence(voterIdBack: VoterIdBack, threshold: Int): Boolean {
    return (voterIdBack.voterid?.confidence ?: 0 >= threshold
            // && voterIdBack.pin?.confidence ?: 0 >= threshold
            && voterIdBack.address?.confidence ?: 0 >= threshold
            // && voterIdBack.type?.confidence ?: 0 >= threshold
            //&& voterIdBack.gender?.confidence ?: 0 >= threshold
            //&& voterIdBack.date?.confidence ?: 0 >= threshold
            //&& voterIdBack.dob?.confidence ?: 0 >= threshold
            //&& voterIdBack.age?.confidence ?: 0 >= threshold
            )
}

/**
 * TODO :
 *
 * @param voterIdFrontNew voterIdFrontNew object
 * @param threshold confidence threshold
 * @return
 */
  internal fun checkVoterIdFrontNewConfidence(voterIdFrontNew: VoterIdFrontNew, threshold: Int): Boolean {
    return (voterIdFrontNew.name?.confidence ?: 0 >= threshold
            && voterIdFrontNew.voterid?.confidence ?: 0 >= threshold
            && voterIdFrontNew.relation?.confidence ?: 0 >= threshold
            )
}

/**
 * TODO :
 *
 * @param voterIdFront voterIdFront object
 * @param threshold confidence threshold
 * @return
 */

  internal fun checkVoterIdFrontConfidence(voterIdFront: VoterIdFront, threshold: Int): Boolean {
    return (voterIdFront.name?.confidence ?: 0 >= threshold
            && voterIdFront.voterid?.confidence ?: 0 >= threshold
            && voterIdFront.dob?.confidence ?: 0 >= threshold
            && voterIdFront.gender?.confidence ?: 0 >= threshold
            // && voterIdFront.doc?.confidence ?: 0 >= threshold
            && voterIdFront.relation?.confidence ?: 0 >= threshold
            // && voterIdFront.age?.confidence ?: 0 >= threshold
            )
}

/**
 * TODO :
 *
 * @param passportBack voterIdFront object
 * @param threshold confidence threshold
 * @return
 */
 internal fun checkPassportBackConfidence(passportBack: PassportBack, threshold: Int): Boolean {
    return (passportBack.address?.confidence ?: 0 >= threshold
            && passportBack.father?.confidence ?: 0 >= threshold
            && passportBack.fileNumber?.confidence ?: 0 >= threshold
            && passportBack.mother?.confidence ?: 0 >= threshold
            //  && passportBack.oldDoi?.confidence ?: 0 >= threshold
            //  && passportBack.oldPassportNumber?.confidence ?: 0 >= threshold
            // && passportBack.oldPlaceOfIssue?.confidence ?: 0 >= threshold
            && passportBack.passportNumber?.confidence ?: 0 >= threshold
            && passportBack.pin?.confidence ?: 0 >= threshold
            // && passportBack.spouse?.confidence ?: 0 >= threshold
            )
}

/**
 * TODO :
 *
 * @param passportFront voterIdFront object
 * @param threshold confidence threshold
 * @return
 */
 internal fun checkPassportFrontConfidence(passportFront: PassportFront, threshold: Int): Boolean {
    return (passportFront.countryCode?.confidence ?: 0 >= threshold
            && passportFront.dob?.confidence ?: 0 >= threshold
            && passportFront.doe?.confidence ?: 0 >= threshold
            && passportFront.doi?.confidence ?: 0 >= threshold
            && passportFront.gender?.confidence ?: 0 >= threshold
            && passportFront.givenName?.confidence ?: 0 >= threshold
            && passportFront.nationality?.confidence ?: 0 >= threshold
            && passportFront.passportNumber?.confidence ?: 0 >= threshold
            && passportFront.placeOfBirth?.confidence ?: 0 >= threshold
            && passportFront.placeOfIssue?.confidence ?: 0 >= threshold
            && passportFront.surname?.confidence ?: 0 >= threshold
            && passportFront.type?.confidence ?: 0 >= threshold
            )
}

/**
 * TODO :
 *
 * @param aadhaarBack aadhaarBack object
 * @param threshold confidence threshold
 * @return
 */
internal  fun checkAadhaarBackConfidence(aadhaarBack: AadhaarBack, threshold: Int): Boolean {
    return (aadhaarBack.aadhaar?.confidence ?: 0 >= threshold
            && aadhaarBack.address?.confidence ?: 0 >= threshold
            && aadhaarBack.father?.confidence ?: 0 >= threshold
            //&& aadhaarBack.husband?.confidence ?: 0 >= threshold
            && aadhaarBack.pin?.confidence ?: 0 >= threshold)
    //&& aadhaarBack.qr?.confidence ?: 0 >= threshold)

}

/**
 * TODO :
 *
 * @param aadhaarFrontTop aadhaarFrontTop object
 * @param threshold confidence threshold
 * @return
 */
  internal fun checkAadhaarFrontTopConfidence(
    aadhaarFrontTop: AadhaarFrontTop,
    threshold: Int
): Boolean {

    return (aadhaarFrontTop.aadhaar?.confidence ?: 0 >= threshold
            && aadhaarFrontTop.address?.confidence ?: 0 >= threshold
            && aadhaarFrontTop.father?.confidence ?: 0 >= threshold
            && aadhaarFrontTop.husband?.confidence ?: 0 >= threshold
            && aadhaarFrontTop.name?.confidence ?: 0 >= threshold
            && aadhaarFrontTop.phone?.confidence ?: 0 >= threshold
            && aadhaarFrontTop.pin?.confidence ?: 0 >= threshold)
}

/**
 * TODO :
 *
 * @param aadhaarFrontBottom aadhaarFrontBottom object
 * @param threshold confidence threshold
 * @return
 */
  internal fun checkAadhaarFrontBottomConfidence(
    aadhaarFrontBottom: AadhaarFrontBottom,
    threshold: Int
): Boolean {

    return (aadhaarFrontBottom.aadhar?.confidence ?: 0 >= threshold
            && aadhaarFrontBottom.dob?.confidence ?: 0 >= threshold
            //&& aadhaarFrontBottom.father?.confidence ?: 0 >= threshold
            && aadhaarFrontBottom.gender?.confidence ?: 0 >= threshold
            // && aadhaarFrontBottom.mother?.confidence ?: 0 >= threshold
            && aadhaarFrontBottom.name?.confidence ?: 0 >= threshold
            //  && aadhaarFrontBottom.yob?.confidence ?: 0 >= threshold
            //    && aadhaarFrontBottom.qr?.confidence ?: 0 >= threshold
            )
}

/**
 * TODO :
 *
 * @param oldPan oldPan object
 * @param threshold confidence threshold
 * @return
 */
 internal fun checkOldPanConfidence(oldPan: OldPan, threshold: Int): Boolean {
    return (oldPan.date?.confidence ?: 0 >= threshold
            && oldPan.father?.confidence ?: 0 >= threshold
            && oldPan.name?.confidence ?: 0 >= threshold
            && oldPan.panNumber?.confidence ?: 0 >= threshold)
}

/**
 * TODO :
 *
 * @param pan pan object
 * @param threshold confidence threshold
 * @return
 */
 internal fun checkPanConfidence(pan: Pan, threshold: Int): Boolean {
    return (pan.date?.confidence ?: 0 >= threshold
            // && pan.dateOfIssue?.confidence ?: 0 >= threshold
            && pan.father?.confidence ?: 0 >= threshold
            && pan.name?.confidence ?: 0 >= threshold
            && pan.panNumber?.confidence ?: 0 >= threshold)
}

/**
 * TODO :
 *
 * @param faceMatchResult faceMatchResult object
 * @param threshold confidence threshold
 * @return
 */
  internal fun isFaceMatchConfidenceGood(faceMatchResult: FaceMatchResult, threshold: Int): Boolean {

    return faceMatchResult.conf ?: 0 >= threshold

}
