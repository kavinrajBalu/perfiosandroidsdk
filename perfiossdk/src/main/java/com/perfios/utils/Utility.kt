package com.perfios.utils

import android.app.Activity
import android.content.Context
import android.location.LocationManager
import android.os.Build
import androidx.appcompat.app.AlertDialog
import androidx.core.location.LocationManagerCompat
import co.hyperverge.hypersnapsdk.objects.HVDocConfig
import co.hyperverge.hypersnapsdk.objects.HyperSnapParams.Region
import com.perfios.objects.DocumentType
import com.perfios.objects.InstancePool
import com.perfios.perfiossdk.BuildConfig
import com.perfios.perfiossdk.R
import org.json.JSONObject
import perfios.commons.codec.binary.Hex
import java.nio.charset.StandardCharsets
import java.security.KeyFactory
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.text.SimpleDateFormat
import java.util.*
import javax.crypto.Cipher
import javax.crypto.Mac
import javax.crypto.spec.SecretKeySpec


lateinit var alterDialog :AlertDialog

/**
 * Get the current time with time zone dd-MM-yyyy HH:mm:ss z
 */
  internal fun getCurrentDateTime(): String {
    val formatter = SimpleDateFormat("dd-MM-yyyy HH:mm:ss z", Locale.getDefault())
    val date = Date(System.currentTimeMillis())
    return formatter.format(date)
}

/**
 * Generate Unique transaction ID.
 */
  internal fun getUniqueTransactionID(): String {
    return UUID.randomUUID().toString()
}

/**
 * Encode data into Base64
 * @param data : ByteArray data
 */
  internal fun encodeDataIntoBase64(data: ByteArray): String {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Base64.getEncoder().encodeToString(data)
    } else {
        android.util.Base64.encodeToString(data, android.util.Base64.NO_WRAP)
    }
}

/**
 * encode header using HmacSHA256 algorithm
 * @param data : string data
 * @return : encrypted data
 */
 internal fun encodeHeader(data: String): String {
    val  key = BuildConfig.HEADER_KEY
    val algorithm = "HmacSHA256"
    val characterSetName = "UTF-8"
    val sha256HMAC = Mac.getInstance(algorithm)

    val secretKey = SecretKeySpec(key.toByteArray(charset(characterSetName)), algorithm)

    sha256HMAC.init(secretKey)

    return encodeDataIntoBase64(
        (sha256HMAC.doFinal(
            ("PERFIOS-HMACSHA256 $data").toByteArray(
                StandardCharsets.UTF_8
            )
        ))
    )
}

/**
 * Encode request body data using RSA algorithm
 * @param data : string data
 * @return : encrypted data
 */
  internal fun encodeData(data: String): String {
    val key =BuildConfig.ENCRYPTION_KEY
    val crytoTransform = "RSA/None/PKCS1Padding"
    val cryptoMethod = "RSA"
    val spec = X509EncodedKeySpec(android.util.Base64.decode(key.trim(), android.util.Base64.DEFAULT))
    val keyFactory = KeyFactory.getInstance(cryptoMethod)
    val cipher: Cipher = Cipher.getInstance(crytoTransform)
    val publicKey = keyFactory.generatePublic(spec)
    cipher.init(Cipher.ENCRYPT_MODE,publicKey)
    return Hex.encodeHexString(cipher.doFinal(data.toByteArray(StandardCharsets.UTF_8)))
}

/**
 * Method used to decode data received from perfios
 * @param data  : string data need to be decrypted
 * @return : return back decoded string
 */
 internal fun decodeData(data:String) : String
{
    val key =BuildConfig.DECRYPTION_KEY
    val crytoTransform = "RSA/None/PKCS1Padding"
    val cryptoMethod = "RSA"
    val spec = PKCS8EncodedKeySpec(android.util.Base64.decode(key.trim(), android.util.Base64.DEFAULT))
    val keyFactory = KeyFactory.getInstance(cryptoMethod)
    val cipher: Cipher = Cipher.getInstance(crytoTransform)
    val privateKey = keyFactory.generatePrivate(spec)
    cipher.init(Cipher.DECRYPT_MODE,privateKey)
    val hexDecodedData = Hex.decodeHex(data.toCharArray())
    return cipher.doFinal(hexDecodedData).toString(StandardCharsets.UTF_8)
}


/**
 * Customize document configuration capture message
 *
 * @param context : client app context
 * @param documentType : document type selected by the user For ex:  AADHAR,PAN,PASSPORT,VOTERID.
 * @param isCaptureBackSide : if capturing document back side make it true or else false.
 * @return customized document configuration.
 */
  internal fun getDocConfig(
    context: Context,
    documentType: DocumentType?,
    isCaptureBackSide: Boolean
): HVDocConfig {
    val docConfig = HVDocConfig()
    docConfig.isShouldShowFlashIcon = false
    docConfig.docReviewDescription = context.getString(R.string.docReviewDescription)
    docConfig.docReviewTitle = context.getString(R.string.docReviewTitle)
    docConfig.setDocCaptureTitle(context.getString(R.string.docCaptureTitle))
    docConfig.docCaptureDescription = context.getString(R.string.docCaptureDescription)
    docConfig.setShouldShowReviewScreen(true)

    docConfig.isShouldShowInstructionPage = true
    // since voterID needs to be taken entirely extending camera view to A4
    when (documentType!!.name) {
        DocumentType.VOTERID.name ->
            docConfig.setDocumentType(HVDocConfig.Document.A4)
        else -> docConfig.setDocumentType(HVDocConfig.Document.CARD)
    }

    //Configure text based on the portion of document.
    if (isCaptureBackSide) {
        docConfig.docCaptureSubText = context.getString(R.string.docCaptureBackSide)
    } else {
        docConfig.docCaptureSubText = context.getString(R.string.docCaptureSubText)
    }
    return docConfig
}


/**
 * @param context current activity context
 * @return  return country
 */
internal  fun getLocale(context: Context): Region {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        when {
            context.resources.configuration.locales.get(0).country.contains(
                "IN",
                true
            ) -> Region.India
            context.resources.configuration.locales.get(0).country.contains(
                "US",
                true
            ) -> Region.UnitedStates
            else -> Region.AsiaPacific
        }
    } else {
        when {
            context.resources.configuration.locale.country.contains("IN", true) -> Region.India
            context.resources.configuration.locale.country.contains(
                "US",
                true
            ) -> Region.UnitedStates
            else -> Region.AsiaPacific
        }
    }
}

/**
 * TODO check location enablement for the app
 *
 * @param context : activity context
 * @return : if enabled return true else false
 */
 internal fun isLocEnabled(context: Context): Boolean {

    val locationManager =  context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    return LocationManagerCompat.isLocationEnabled(locationManager)
}

/**
 * TODO common progress dialog.To dismiss progress dialog call dismissProgressDialog().
 *
 * @param context : activity context
 */
 internal fun showProgressDialog(context: Context)
{
    val builder = AlertDialog.Builder(context)
    builder.setCancelable(false)
    builder.setView(R.layout.progress_dialog)
    alterDialog= builder.create()
    alterDialog.show()
}

/**
 * TODO : dismiss progress dialog.
 *
 */
internal  fun dismissProgressDialog()
{
    alterDialog.dismiss()
}

/**
 * TODO alter dialog specifically used for ocr result screen
 *
 * @param context activity context
 * @param message message to be displayed
 * @param title title for the message
 * @return
 */
 internal fun showAlertDialog(context : Context, message : String?, title : String?) : AlertDialog.Builder
{
    val builder = AlertDialog.Builder(context)
    builder.setCancelable(false)
    builder.setMessage(message?:"Document is not clear.Capture again with glare free and not blurred.")
    builder.setTitle(title?:"Recapture Document")
    builder.create()
    return builder
}

/**
 * TODO if user exceeds the retry limit then show this dialog
 *
 * @param context : activity context
 */
internal fun showRetryExceedDailog(context:Context)
{
    showAlertDialog(
        context,
        "Exceeded the retry limit.Canceling transaction.",
        context.getString(R.string.message_title)
    ).setPositiveButton(context.resources.getString(R.string.yes)) { dialog, _ ->
        InstancePool.kycResultCallBack?.onFailed("user exceeded retry.", null)
        dialog.dismiss()
        (context as Activity).finish()
    }.show()
}

/**
 * TODO get the exact card type for extracting ocr result
 *
 * @param result ocr result json object
 * @return card type as string
 */
 internal fun getExactCardType(result: JSONObject?):String
{
    return (result?.getJSONArray("result")?.get(0) as JSONObject).get("type").toString()
}

/**
 * TODO Extract result from the JSON response
 *
 * @param result ocr result json object
 * @return
 */
 internal fun getCardResultJSON(result: JSONObject?):String
{
    return (result?.getJSONArray("result")?.get(0) as JSONObject).getJSONObject("details").toString()
}

/**
 * TODO get Face Match Result
 *
 * @param result face match result JSON object
 * @return  extracted result object
 */
  fun getFaceMatchResult(result: JSONObject?):String
{
    return (result?.getJSONObject("result").toString())
}
