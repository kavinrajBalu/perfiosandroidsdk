package com.perfios.utils

import android.content.Context
import android.location.Address
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.perfios.objects.InstancePool

/**
 * TODO  : GeoLocationDataReceiver to fetch the current location of the user.
 *
 * @property context : client application context
 */
 internal class GeoLocationDataReceiver(var context: Context) {

    fun getCurrentLocation(): String {
        var latitude = ""
        var longitude = ""
        val accuracy = ""
        val addresses: List<Address>? = null

        val mLocationRequest = LocationRequest.create()
        mLocationRequest.interval = 60000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY


        val mLocationCallback: LocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                for (location in locationResult.locations) {
                    if (location != null) {
                        latitude = location.latitude.toString()
                        longitude = location.longitude.toString()
                       /* accuracy = location.accuracy.toString()
                        val geocode = Geocoder(context, Locale.getDefault())
                        addresses =
                            geocode.getFromLocation(latitude.toDouble(), longitude.toDouble(), 1)*/
                        InstancePool.location = "${latitude.toDouble()},${longitude.toDouble()}"
                      /*  Toast.makeText(context," ${latitude.toDouble()},${longitude.toDouble()}",
                            Toast.LENGTH_SHORT).show()*/
                    }
                }
            }
        }
        LocationServices.getFusedLocationProviderClient(context)
            .requestLocationUpdates(mLocationRequest, mLocationCallback, null)
        return " $accuracy \n\n ${addresses?.get(0)?.getAddressLine(0)} \n\n $latitude , $longitude"
    }

}