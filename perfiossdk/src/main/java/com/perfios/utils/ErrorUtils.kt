package com.perfios.utils

import com.perfios.objects.APIError
import com.perfios.objects.LoginResponse
import com.perfios.retrofit.PerfiosRetroClient
import retrofit2.Response


internal  object ErrorUtils {
    fun parseError(response: Response<LoginResponse>): APIError? {
        return PerfiosRetroClient.getClient()?.responseBodyConverter<APIError>(
            APIError::class.java,
            arrayOf()
        )?.convert(response.errorBody())
    }
}