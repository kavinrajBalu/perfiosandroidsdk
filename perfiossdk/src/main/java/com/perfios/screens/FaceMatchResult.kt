package com.perfios.screens

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.perfios.adapter.ResultDataAdapter
import com.perfios.objects.InstancePool
import com.perfios.perfiossdk.R
import com.perfios.perfiossdk.databinding.ActivityFaceMatchResultBinding
import com.perfios.utils.dismissProgressDialog
import com.perfios.utils.showAlertDialog
import com.perfios.utils.showProgressDialog
import com.perfios.utils.showRetryExceedDailog
import com.perfios.viewModel.FaceMatchResultViewModel
import java.util.*

/**
 * TODO Face match result screen
 *
 */
  internal class FaceMatchResult : AppCompatActivity() {

    lateinit var  binding: ActivityFaceMatchResultBinding

    private lateinit var viewModel : FaceMatchResultViewModel

    private var mRecyclerView: RecyclerView? = null

    private var mAdapter: ResultDataAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

         val islivenessFailed = intent.getBooleanExtra(resources.getString(R.string.livenessFailed_key),false)

        //Data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_face_match_result)

        if(!islivenessFailed) {
            //ViewModel
            viewModel = ViewModelProviders.of(this).get(FaceMatchResultViewModel::class.java)


            //Show progress dialog
            showProgressDialog(this)


            viewModel.resultMap.observe(this, Observer { result ->
                dismissProgressDialog()
                populateUI(result)
            })


            //error message listener
            viewModel.faceMatchError.observe(this, Observer { error ->
                if (InstancePool.retryCounter <= InstancePool.perfiosSDK?.loginResponse?.retry!!) {
                    InstancePool.retryCounter = InstancePool.retryCounter + 1
                    dismissProgressDialog()
                    var errorMessage : String
                    var errorAction  : String
                    if (error.contains("internet")){
                        errorMessage =error
                        errorAction  = getString(R.string.ok)

                    }
                    else  {
                        errorMessage = "$error.Redo document capture."
                        errorAction  = getString(R.string.yes)
                    }

                    showAlertDialog(
                        this@FaceMatchResult,
                        errorMessage,
                        getString(R.string.error_title)
                    ).setPositiveButton(errorAction) { dialog, _ ->
                        if (error == getString(R.string.face_error)) {
                            InstancePool.perfiosSDK?.recaptureDocument()
                        } else {
                            InstancePool.perfiosSDK?.startFaceCapture()
                        }
                        dialog.dismiss()
                        finish()
                    }.show()
                } else {
                    showRetryExceedDailog(this)
                }
            })
        }
        else
        {
            if (InstancePool.retryCounter <= InstancePool.perfiosSDK?.loginResponse?.retry!!) {
                InstancePool.retryCounter = InstancePool.retryCounter + 1
                showAlertDialog(
                    this@FaceMatchResult,
                    getString(R.string.face_not_clear),
                    getString(R.string.error_title)
                ).setPositiveButton(resources.getString(R.string.yes)) { dialog, _ ->
                    dialog.dismiss()
                    InstancePool.perfiosSDK?.startFaceCapture()
                    finish()
                }.show()
            } else {
                showRetryExceedDailog(this)
            }
        }


        binding.nextButton.setOnClickListener {

            InstancePool.perfiosSDK?.sendFinalData(this, InstancePool.perfiosSDK?.finalPerfiosJsonObject!!)

            if (InstancePool.perfiosSDK?.loginResponse?.isLiveness == true) {
                InstancePool.kycResultCallBack?.onSuccess(
                    InstancePool.perfiosSDK?.clubbedResponse,
                    InstancePool.perfiosSDK?.faceMatchJsonObject,
                    InstancePool.perfiosSDK?.liveness,
                    InstancePool.perfiosSDK?.faceImageURI ?:""
                )
                finish()
            } else {
                InstancePool.kycResultCallBack?.onSuccess(
                    InstancePool.perfiosSDK?.clubbedResponse,
                    InstancePool.perfiosSDK?.faceMatchJsonObject,
                    null,
                    InstancePool.perfiosSDK?.faceImageURI ?:""
                )
                finish()
            }
        }


        binding.retry.setOnClickListener {
            if(InstancePool.retryCounter<=InstancePool.perfiosSDK?.loginResponse?.retry!!) {
                InstancePool.retryCounter = InstancePool.retryCounter + 1
                InstancePool.perfiosSDK?.startFaceCapture()
                InstancePool.perfiosSDK?.loginResponse?.retry = InstancePool.perfiosSDK?.loginResponse?.retry!!.minus(1)
                finish()
            }
            else
            {
                showRetryExceedDailog(this)
            }
        }
    }

    private fun populateUI(result: LinkedHashMap<String, String>) {

        // Create an adapter and supply the data to be displayed.
        mAdapter = ResultDataAdapter(this, result)

        mRecyclerView = binding.faceMatchRecylerview

        mRecyclerView?.layoutManager = LinearLayoutManager(this)

        mRecyclerView?.adapter = mAdapter

    }

    override fun onBackPressed() {
        super.onBackPressed()
        InstancePool.kycResultCallBack?.onFailed(getString(R.string.perfios_back_pressed),null)
    }
}
