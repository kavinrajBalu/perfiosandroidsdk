package com.perfios.screens

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.perfios.objects.DocType
import com.perfios.objects.DocumentType
import com.perfios.objects.InstancePool
import com.perfios.perfiossdk.R
import com.perfios.perfiossdk.databinding.ActivitySelectDocumentBinding

/**
 * TODO : select document screen.
 *
 */
  internal class SelectDocument : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivitySelectDocumentBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_select_document)
            binding.confirmButton.setOnClickListener {

                InstancePool.perfiosSDK?.sendDocumentTypeToPerfios(
                    DocType(
                        getDocumentType(binding).name,
                        getString(R.string.perfios_true),
                        ""
                    )
                )
            InstancePool.perfiosSDK!!.startAppropriateDocumentActivity(
                this,
                com.perfios.utils.getDocConfig(this,getDocumentType(binding),false),
                getDocumentType(binding))
                finish()
        }
    }

    private fun getDocumentType(binding: ActivitySelectDocumentBinding): DocumentType {
        return when (binding.radioGroup.checkedRadioButtonId) {
            binding.voterid.id -> DocumentType.VOTERID
            binding.aadhar.id -> DocumentType.AADHAR
            binding.pan.id -> DocumentType.PAN
            binding.passport.id -> DocumentType.PASSPORT
            else -> DocumentType.AADHAR
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        InstancePool.kycResultCallBack?.onFailed(getString(R.string.perfios_back_pressed),null)
    }
}
