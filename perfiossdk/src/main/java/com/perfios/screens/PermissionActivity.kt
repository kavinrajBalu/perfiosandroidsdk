package com.perfios.screens

import android.Manifest.permission
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.perfios.objects.InstancePool
import com.perfios.perfiossdk.R
import com.perfios.utils.GeoLocationDataReceiver
import com.perfios.utils.isLocEnabled

/**
 * TODO : Permission activity  to show permission request.
 *
 */

  internal class PermissionActivity : AppCompatActivity() {
    private val LOCATION_SETTINGS_REQUEST: Int = 100
    private val MY_PERMISSIONS_REQUEST_LOCATION = 99
    private val REQUEST_LOCATION_PERMISSION_SETTING = 98
    private var isLocationMandatory: String? = null
    private var MAXRETRY = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_permission)

        isLocationMandatory = intent.getStringExtra(getString(R.string.should_show))

        if(checkLocationPermission())
        {
            if(!isLocEnabled(this))
            {
                displayLocationSettingsRequest()
            }
            else
            {
                GeoLocationDataReceiver(this).getCurrentLocation()
                InstancePool.locationCallBack?.isSuccess(true)
                finish()
            }
        }
        else
        {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(permission.ACCESS_FINE_LOCATION),
                MY_PERMISSIONS_REQUEST_LOCATION
            )
        }
    }

    /**
     * TODO  display Location Settings Request
     *
     */
    private fun displayLocationSettingsRequest() {

        val mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(10 * 1000.toLong())
            .setFastestInterval(1 * 1000.toLong())

        val settingsBuilder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        settingsBuilder.setAlwaysShow(true)

        val result = LocationServices.getSettingsClient(this)
                .checkLocationSettings(settingsBuilder.build())

        result.addOnCompleteListener { task ->
            try {
                task.getResult(ApiException::class.java)
            } catch (ex: ApiException) {
                when (ex.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        val resolvableApiException =
                            ex as ResolvableApiException
                        resolvableApiException
                            .startResolutionForResult(
                                this,
                                LOCATION_SETTINGS_REQUEST
                            )
                    } catch (e: SendIntentException) {
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }
            }
        }
    }

    /**
     * TODO : callback method from the OS
     *
     * @param requestCode request code sent from user
     * @param resultCode  result code for result
     * @param data : if any data forwarded by activity will be accesssed through intent
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(REQUEST_LOCATION_PERMISSION_SETTING == requestCode)
        {
            if(!checkLocationPermission())
            {
                if(MAXRETRY>0) {
                    MAXRETRY -= 1
                    AlertDialog.Builder(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert)
                        .setTitle(getString(R.string.permission_required_message))
                        .setMessage(getString(R.string.enable_permission_message))
                        .setCancelable(false)
                        .setPositiveButton(
                            getString(R.string.ok)
                        ) { _, _ ->
                            val intent =
                                Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                            val uri: Uri = Uri.fromParts("package", packageName, null)
                            intent.data = uri
                            startActivityForResult(intent, REQUEST_LOCATION_PERMISSION_SETTING)
                        }.create()
                        .show()
                }
                else
                {
                    InstancePool.locationCallBack?.isSuccess(false)
                    finish()
                }
            }
            else
            {
               if(!isLocEnabled(this))
               {
                   displayLocationSettingsRequest()
               }
            }
        }
        else if(LOCATION_SETTINGS_REQUEST == requestCode)
        {
            if(resultCode == Activity.RESULT_OK)
            {
                GeoLocationDataReceiver(this).getCurrentLocation()
                InstancePool.locationCallBack?.isSuccess(true)
                finish()
            }
            else if(resultCode == Activity.RESULT_CANCELED)
            {
                if(!isLocEnabled(this))
                {
                    if(isLocationMandatory!=null && isLocationMandatory == resources.getString(R.string.yes)) {
                        if(MAXRETRY>0) {
                            MAXRETRY -= 1
                            displayLocationSettingsRequest()
                        }
                        else
                        {
                            InstancePool.locationCallBack?.isSuccess(false)
                            finish()
                        }
                    }
                    else
                    {
                        InstancePool.locationCallBack?.isSuccess(true)
                        finish()
                    }

                }

            }
        }
    }

    /**
     * TODO permission result call back from the OS
     *
     * @param requestCode request code for location permission
     * @param permissions  permission is array of string
     * @param grantResults result int array
     */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                if(!isLocEnabled(this))
                {
                    displayLocationSettingsRequest()
                }
                else
                {
                    GeoLocationDataReceiver(this).getCurrentLocation()
                    InstancePool.locationCallBack?.isSuccess(true)
                    finish()
                }
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission.ACCESS_FINE_LOCATION))
                {
                    // Show an explanation to the user *asynchronously* -- don't block
                    // this thread waiting for the user's response! After the user
                    // sees the explanation, try again to request the permission.
                        if (isLocationMandatory != null && isLocationMandatory == resources.getString(R.string.yes)) {
                            if( MAXRETRY>0){
                            MAXRETRY-=1
                            AlertDialog.Builder(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert)
                                .setTitle("Permission required")
                                .setMessage("KYC needs your permission to detect suspicious activity.")
                                .setCancelable(false)
                                .setPositiveButton(
                                    getString(R.string.ok)
                                ) { _, _ ->
                                    //Prompt the user once explanation has been shown
                                    ActivityCompat.requestPermissions(
                                        this,
                                        arrayOf(permission.ACCESS_FINE_LOCATION),
                                        MY_PERMISSIONS_REQUEST_LOCATION
                                    )
                                }
                                .create()
                                .show()
                            }
                            else
                            {
                                InstancePool.locationCallBack?.isSuccess(false)
                                finish()
                            }
                        } else {
                            InstancePool.locationCallBack?.isSuccess(true)
                            finish()
                        }
                }
                else
                {
                    if(MAXRETRY>0) {
                        MAXRETRY -= 1
                        AlertDialog.Builder(this, R.style.Theme_AppCompat_DayNight_Dialog_Alert)
                            .setTitle("Permission required")
                            .setMessage("Enable permission from settings")
                            .setCancelable(false)
                            .setPositiveButton(
                                getString(R.string.ok)
                            ) { _, _ ->
                                val intent =
                                    Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                                val uri: Uri = Uri.fromParts("package", packageName, null)
                                intent.data = uri
                                startActivityForResult(intent, REQUEST_LOCATION_PERMISSION_SETTING)
                            }.create()
                            .show()
                    }
                    else
                    {
                        InstancePool.locationCallBack?.isSuccess(false)
                        finish()
                    }
                }
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    /**
     * TODO : check whether the permission is granted for the application.
     *
     * @return
     */
    private fun checkLocationPermission(): Boolean {
        return ContextCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }
}
