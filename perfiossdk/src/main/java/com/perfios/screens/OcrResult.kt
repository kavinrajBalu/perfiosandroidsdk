package com.perfios.screens

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.perfios.adapter.ResultDataAdapter
import com.perfios.objects.DocumentType
import com.perfios.objects.InstancePool
import com.perfios.perfiossdk.R
import com.perfios.perfiossdk.databinding.ActivityOcrResultBinding
import com.perfios.utils.dismissProgressDialog
import com.perfios.utils.showAlertDialog
import com.perfios.utils.showProgressDialog
import com.perfios.utils.showRetryExceedDailog
import com.perfios.viewModel.OcrResultViewModel
import java.util.*

/**
 * TODO : Ocr result screen
 *
 */

  internal class OcrResult : AppCompatActivity() {

    private lateinit var viewModel : OcrResultViewModel
    private lateinit var  binding: ActivityOcrResultBinding
    private var mRecyclerView: RecyclerView? = null
    private var mAdapter: ResultDataAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Data binding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_ocr_result)


        setScreenTitle(InstancePool.perfiosSDK?.documentType)

        //ViewModel
        viewModel = ViewModelProviders.of(this).get(OcrResultViewModel::class.java)


        //Show progress dialog
        showProgressDialog(this)


        //Observe successful result using live data
        viewModel.resultMap.observe(this, Observer {result ->
//            Log.i("OcrResult","OcrResult received!----> $result")

            dismissProgressDialog()

            //Todo : populate UI elements here
            populateUI(result)
        } )


        //Observe failure result using live data
        viewModel.ocrError.observe(this, Observer { error ->
            //            Log.i("OcrResult","ocr failed----> $error")
            dismissProgressDialog()
            if(InstancePool.retryCounter<=InstancePool.perfiosSDK?.loginResponse?.retry!!) {
                var errorMessage : String
                var errorAction  : String

                if (error.contains("internet")){
                    errorMessage =error
                    errorAction  = getString(R.string.ok)

                }
                else  {
                    errorMessage = "$error.Redo document capture."
                    errorAction  = getString(R.string.yes)
                }
            showAlertDialog(
                this@OcrResult,
                 errorMessage,
                getString(R.string.error_title)
            ).setPositiveButton(errorAction) { dialog, _ ->
                    InstancePool.retryCounter = InstancePool.retryCounter + 1
                    if (InstancePool.perfiosSDK?.docConfig?.docCaptureSubText == getString(R.string.docCaptureBackSide)) {
                        InstancePool.perfiosSDK?.captureDocumentBackside(
                            this,
                            InstancePool.perfiosSDK?.documentType!!
                        )
                    } else {
                        InstancePool.perfiosSDK?.recaptureDocument()
                    }
                    dialog.dismiss()
                    finish()
            }.show()
        }
            else {
                showRetryExceedDailog(this)
            }
        } )



        //Next button action
        binding.nextButton.setOnClickListener {

            if(InstancePool.perfiosSDK?.documentType!!.name != DocumentType.PAN.name)
            {
                if(InstancePool.perfiosSDK?.docConfig?.docCaptureSubText == getString(R.string.docCaptureSubText))
                {
                    InstancePool.perfiosSDK?.captureDocumentBackside(this, InstancePool.perfiosSDK?.documentType!!)
                }
                else
                {

                    performNextAction()

                }
            }
            else
            {
                performNextAction()
            }

            finish()
        }

        //Finish button action
        binding.retry.setOnClickListener {
            if(InstancePool.retryCounter<=InstancePool.perfiosSDK?.loginResponse?.retry!!) {
                InstancePool.retryCounter = InstancePool.retryCounter + 1
                deletePreviousDataFromCluddedResponse()

                if (InstancePool.perfiosSDK?.docConfig?.docCaptureSubText == getString(R.string.docCaptureBackSide)) {
                    InstancePool.perfiosSDK?.captureDocumentBackside(
                        this,
                        InstancePool.perfiosSDK?.documentType!!
                    )
                } else {
                    InstancePool.perfiosSDK?.recaptureDocument()
                }

                finish()
            }
            else
            {
                showRetryExceedDailog(this)
            }
        }
    }

    private fun deletePreviousDataFromCluddedResponse() {

        val size = InstancePool.perfiosSDK?.clubbedResponse?.length()?:0
        if(size>0) {
            InstancePool.perfiosSDK?.clubbedResponse?.remove(size.minus(1))
        }

    }

    private fun setScreenTitle(documentType: DocumentType?) {

        when {
            documentType.toString() == DocumentType.AADHAR.toString() -> {
                binding.title.text = documentType.toString()+" data"
            }
            documentType.toString() == DocumentType.PAN.toString() -> {
                binding.title.text = documentType.toString()+" data"
            }
            documentType.toString() == DocumentType.PASSPORT.toString() -> {
                binding.title.text = documentType.toString()+" data"
            }
            documentType.toString() == DocumentType.VOTERID.toString() -> {
                binding.title.text = documentType.toString()+" data"
            }
        }
    }

    private fun performNextAction() {
        if (InstancePool.perfiosSDK?.loginResponse?.isFaceMatch == true || InstancePool.perfiosSDK?.loginResponse?.isLiveness == true) {
            InstancePool.perfiosSDK?.startFaceCapture()
        }
        else
        {
            InstancePool.perfiosSDK?.sendFinalData(this, InstancePool.perfiosSDK?.finalPerfiosJsonObject!!)

            InstancePool.kycResultCallBack?.onSuccess(
                InstancePool.perfiosSDK?.clubbedResponse,
                null,
                null,
                InstancePool.perfiosSDK?.frontDocumentImageURI!!
            )
        }
    }

    private fun populateUI(result: LinkedHashMap<String, String>) {

        // Create an adapter and supply the data to be displayed.
        mAdapter = ResultDataAdapter(this, result)

        mRecyclerView = binding.ocrRecylerview

        mRecyclerView?.layoutManager = LinearLayoutManager(this)

        mRecyclerView?.adapter = mAdapter

        if (InstancePool.perfiosSDK?.loginResponse?.isFaceMatch != true && InstancePool.perfiosSDK?.loginResponse?.isLiveness != true) {

            binding.nextButton.text = getString(R.string.finish)
        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
        InstancePool.kycResultCallBack?.onFailed(getString(R.string.perfios_back_pressed),null)
    }
}
