package com.perfios.screens

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.perfios.adapter.ResultDataAdapter
import com.perfios.objects.InstancePool
import com.perfios.objects.LivenessResult
import com.perfios.perfiossdk.R
import com.perfios.perfiossdk.databinding.ActivityLivenessResultBinding
import com.perfios.utils.showRetryExceedDailog
import com.perfios.viewModel.LivenessViewModel
import com.perfios.viewModel.LivenessViewModelFactory
import org.json.JSONObject
import java.util.*

/**
 * TODO liveness result screen.
 *
 */
 internal class LivenessResult : AppCompatActivity() {

    private  var  livenessJsonObject : JSONObject? = null

    private lateinit var  binding: ActivityLivenessResultBinding

    private var mRecyclerView: RecyclerView? = null

    private var mAdapter: ResultDataAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         binding = DataBindingUtil.setContentView(this, R.layout.activity_liveness_result)

          val result = intent.getStringExtra(getString(R.string.livenessResult))

          livenessJsonObject = JSONObject(result)

          val livenessResult = Gson().fromJson(livenessJsonObject?.get(getString(R.string.result)).toString(),LivenessResult::class.java)

          val viewModelFactory = LivenessViewModelFactory(livenessResult,this)

          val livenessViewModel = ViewModelProviders.of(this,viewModelFactory).get(LivenessViewModel::class.java)

          uiActionListeners(binding)

        livenessViewModel.livenessResultLiveData.observe(this, Observer { livenessResult->

            updateView(livenessResult)
        })

    }

    private fun updateView(livenessResult: LinkedHashMap<String, String>) {

        // Create an adapter and supply the data to be displayed.
        mAdapter = ResultDataAdapter(this, livenessResult)

        mRecyclerView = binding.faceMatchRecylerview

        mRecyclerView?.layoutManager = LinearLayoutManager(this)

        mRecyclerView?.adapter = mAdapter


    }

    private fun uiActionListeners(binding: ActivityLivenessResultBinding) {
        binding.finishButton.setOnClickListener {

            InstancePool.perfiosSDK?.sendFinalData(this, InstancePool.perfiosSDK?.finalPerfiosJsonObject!!)

            InstancePool.kycResultCallBack?.onSuccess(
                InstancePool.perfiosSDK?. clubbedResponse,
                InstancePool.perfiosSDK?.faceMatchJsonObject,
                livenessJsonObject,
                InstancePool.perfiosSDK?.faceImageURI ?:""
            )
            finish()
        }

        binding.retry.setOnClickListener {
            if(InstancePool.retryCounter<= InstancePool.perfiosSDK?.loginResponse?.retry!!) {
                InstancePool.retryCounter = InstancePool.retryCounter + 1
                InstancePool.perfiosSDK?.startFaceCapture()
                finish()
            }
            else
            {
                showRetryExceedDailog(this)
            }
        }
    }



    override fun onBackPressed() {
        super.onBackPressed()
        InstancePool.kycResultCallBack?.onFailed(getString(R.string.perfios_back_pressed),null)
    }
}

