package com.perfios.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.perfios.perfiossdk.R
import kotlinx.android.synthetic.main.list_item.view.*
import java.util.*

/**
 * TODO Result Adapter is common for all the result screens.
 *
 * @property context : Context of the current activity.
 * @property names : ArrayList items to be displayed on the result screen.
 */
class FinalResultDataAdapter(private val context:Context, private val names : ArrayList<String>, private val values : ArrayList<String>) : RecyclerView.Adapter<FinalResultDataAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {
       return ViewHolder(LayoutInflater.from(context).inflate(R.layout.list_item, parent, false))
    }

    override fun getItemCount(): Int {
        return names.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = names[position]
        holder.value.text = if(values[position].isBlank()) context.getString(R.string.empty_value_alternator) else values[position]
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name = view.name!!
        val value = view.value!!
    }
}