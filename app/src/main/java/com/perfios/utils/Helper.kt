package com.perfios.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import com.perfios.perfioslibrary.R

lateinit var alterDialog :AlertDialog
fun showProgressDialog(context: Context)
{
    val builder = AlertDialog.Builder(context)
    builder.setCancelable(false)
    builder.setView(R.layout.progress_dialog)
    alterDialog= builder.create()
    alterDialog.show()
}

fun dismissProgressDialog()
{
    alterDialog.dismiss()
}
