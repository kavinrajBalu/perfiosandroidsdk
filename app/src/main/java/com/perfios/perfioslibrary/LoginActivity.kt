package com.perfios.perfioslibrary

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.perfios.interfaces.KYCResultCallBack
import com.perfios.perfios.Perfios
import com.perfios.perfioslibrary.databinding.ActivityLoginBinding
import com.perfios.utils.dismissProgressDialog
import com.perfios.utils.showProgressDialog
import org.json.JSONArray
import org.json.JSONObject


class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding : ActivityLoginBinding =  DataBindingUtil.
            setContentView(this, R.layout.activity_login)

        //TODO : Setting Default values only for demo.
        binding.uuidEditText.setText("4cdbd121-5644-11e7-8dbd-0242ac110003")

        binding.secretEditText.setText("abcd1234")

        binding.orgEditText.setText("acme")

        binding.loginButton.setOnClickListener {
            //Validate feilds
             val isFieldValidationPass : Boolean  = validateFields(binding)

            // if UUID,secret and org is available make perfios init and fire document capture.

            //TODO : remove ! from if to validate fields.
            if(isFieldValidationPass) {
                showProgressDialog(this)
                startPerfios(binding.uuidEditText.text.toString(),binding.secretEditText.text.toString(),getOrganization(binding))

            }
        }
    }

    private fun startPerfios(
        clientId: String,
        secret: String,
        organization: String
    ) {
        Perfios.init(this,clientId,secret, organization, object : KYCResultCallBack{
            override fun onSuccess(
                ocrResult: JSONArray?,
                faceMatchResult: JSONObject?,
                livenessResult: JSONObject?,
                imageURI: String
            ) {
                dismissProgressDialog()
                val intent = Intent(baseContext, ResultActivity::class.java)
                intent.putExtra(getString(R.string.face_match_result), faceMatchResult?.toString())
                intent.putExtra(getString(R.string.livenessResult),livenessResult?.toString())
                intent.putExtra(getString(R.string.ocr_result),ocrResult?.toString())
                intent.putExtra(getString(R.string.imageUri),imageURI)
                startActivity(intent)
            }
            override fun onFailed(error: String, t: Throwable?) {
                dismissProgressDialog()
                Toast.makeText(applicationContext,error, Toast.LENGTH_LONG).show()
             /*  if(error.equals(getString(R.string.back_clicked_string),true)
                    ||error.equals("Login Failed",true)) {
                    Toast.makeText(applicationContext,error,Toast.LENGTH_SHORT).show()
                }
                else
                {
                    val intent = Intent(baseContext, ResultActivity::class.java)
                    intent.putExtra(getString(R.string.error), error)
                    startActivity(intent)
                }*/
            }
        })



    }

    private fun getOrganization(binding: ActivityLoginBinding): String {
        return binding.orgEditText.text!!.toString()
        }

    private fun validateFields(binding : ActivityLoginBinding): Boolean {
        var isFeildNotEmpty =true
        if(binding.uuidEditText.text.toString().isEmpty())
        {
            binding.uuidTextInput.error = getString(R.string.enter_uuid)
            isFeildNotEmpty= false
        }
        if(binding.secretEditText.text.toString().isEmpty())
        {
            binding.secretTextInput.error = getString(R.string.enter_password)
            isFeildNotEmpty= false
        }
        if(binding.orgEditText.text.toString().isEmpty())
        {
            binding.orgTextInput.error=getString(R.string.enter_organization)
            isFeildNotEmpty= false
        }
        return isFeildNotEmpty
    }
}
