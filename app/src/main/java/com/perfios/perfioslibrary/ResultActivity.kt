package com.perfios.perfioslibrary

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.perfios.adapter.FinalResultDataAdapter
import com.perfios.card.objects.*
import com.perfios.objects.DocumentTypeResponseNames
import com.perfios.objects.FaceMatchResult
import com.perfios.objects.LivenessResult
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject
import java.io.File


class ResultActivity : AppCompatActivity() {

    lateinit var livenessData: TextView

    private lateinit var faceImage:ImageView

    private lateinit var back:ImageView

    private var mRecyclerView: RecyclerView? = null

    private var resultMap = HashMap<String,String>()

    private var names = ArrayList<String>()

    private var values = ArrayList<String>()

    private var mAdapterFinal: FinalResultDataAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        //TODO Fire login activity for demo purpose
       // Perfios.startLoginActivity(this)
        faceImage = findViewById(R.id.avatar)
        back =  findViewById(R.id.back)
        val faceMatchResult = intent.getStringExtra(getString(R.string.face_match_result))
        val ocrResult = intent.getStringExtra(getString(R.string.ocr_result))
        val error = intent.getStringExtra(getString(R.string.error))
        val imageURI = intent.getStringExtra(getString(R.string.imageUri))
        val livenessResult = intent.getStringExtra(getString(R.string.livenessResult))


        if(!imageURI.isNullOrEmpty())
        {
            Picasso.get().load(File(imageURI)).into(faceImage)
        }

        if(ocrResult!=null && ocrResult !="[]") {
            //OCR result
            val ocrObject = JSONArray(ocrResult)

            val cardType = getExactCardType(ocrObject, 0)

            val jsonCardData = getCardResultJSON(ocrObject, 0)

            resultMap.putAll(getCardValueInHashMap(jsonCardData, cardType)!!)

            val keys: Set<String> = resultMap.keys
            val values: MutableCollection<String> = resultMap.values

            for (key in keys) {
                names.add(key)
            }

            for (value in values) {
                this.values.add(value)
            }
            resultMap.clear()

            if (cardType != "pan") {
                val jsonCardData = getCardResultJSON(ocrObject, 1)

                val cardType = getExactCardType(ocrObject, 1)

                resultMap.putAll(getCardValueInHashMap(jsonCardData, cardType)!!)

                val keys: Set<String> = resultMap.keys
                val values: MutableCollection<String> = resultMap.values

                for (key in keys) {
                    names.add(key)
                }

                for (value in values) {
                    this.values.add(value)
                }
                resultMap.clear()
            }

        }
        if(faceMatchResult!=null && faceMatchResult!="{}") {
            //FaceMatch
            val faceResult = JSONObject(faceMatchResult)

            val rawfaceResult = getFaceMatchResult(faceResult)

            val faceObject = Gson().fromJson(rawfaceResult, FaceMatchResult::class.java)

            resultMap.putAll(getFaceMatchMap(faceObject))

            val keys2: Set<String> = resultMap.keys
            val values2: MutableCollection<String> = resultMap.values

            for (key in keys2) {
                names.add(key)
            }

            for (value in values2) {
                this.values.add(value)
            }
            resultMap.clear()


        }

        if(livenessResult!=null) {
            //Liveness Result
            val livenessJsonObject = JSONObject(livenessResult)

            val livenessResultObject = Gson().fromJson(
                livenessJsonObject.get("result").toString(),
                LivenessResult::class.java
            )

            resultMap.putAll(getLivenessMap(livenessResultObject))

            val keys3: Set<String> = resultMap.keys
            val values3: MutableCollection<String> = resultMap.values

            for (key in keys3) {
                names.add(key)
            }

            for (value in values3) {
                this.values.add(value)
            }
            resultMap.clear()
        }
        updateView(resultMap)

        back.setOnClickListener { onBackPressed() }

    }

    private fun updateView(finalResult: java.util.HashMap<String, String>) {

        // Create an adapter and supply the data to be displayed.
        mAdapterFinal = FinalResultDataAdapter(this, names,values)

        mRecyclerView = findViewById(R.id.recyleView)

        mRecyclerView?.layoutManager = LinearLayoutManager(this)

        mRecyclerView?.adapter = mAdapterFinal


    }

    private fun getCardResultJSON(result: JSONArray?, index:Int):String
    {
        return (((result?.get(index) as JSONObject).get("result") as JSONArray).get(0) as JSONObject).get("details").toString()
       // return ((((result?.get("ocrdata") as JSONArray).get(index) as JSONObject).get("result") as JSONArray).get(0) as JSONObject).get("details").toString()
    }

    /**
     * TODO : Method used to parse JSON object to custom object class.
     *
     * @param result : JSON object.
     * @param cardType : Card type received from response.
     * @return  return hashmap value .
     */
    private fun getCardValueInHashMap(result: String, cardType:String):HashMap<String,String>?
    {

        when (cardType) {
            DocumentTypeResponseNames.pan.toString() ->{

                val pan = Gson().fromJson(result, Pan::class.java)

                return getMapValueforPan(pan)


            }

            DocumentTypeResponseNames.old_pan.toString() -> {
                val pan = Gson().fromJson(result, OldPan::class.java)
                return getMapValueforOldPan(pan)
            }

            DocumentTypeResponseNames.aadhaar_front_bottom.toString() -> {

                val aadhar =Gson().fromJson(result, AadhaarFrontBottom::class.java)
                return getMapValueForAadharFrontBottom(aadhar)
            }

            DocumentTypeResponseNames.aadhaar_back.toString() -> {

                val aadhar = Gson().fromJson(result, AadhaarBack::class.java)
                return getMapValueForAadharBack(aadhar)
            }

            DocumentTypeResponseNames.aadhaar_front_top.toString() -> {

                val aadhar =  Gson().fromJson(result, AadhaarFrontTop::class.java)
                return  getMapValueForAadharFrontTop(aadhar)
            }

            DocumentTypeResponseNames.aadhaar_back.toString() -> {

                val aadhar = Gson().fromJson(result, AadhaarBack::class.java)
                return getMapValueForAadharBack(aadhar)
            }

            DocumentTypeResponseNames.passport_front.toString() -> {
                val passport = Gson().fromJson(result, PassportFront::class.java)
                return getMapValueForPassportFront(passport)
            }

            DocumentTypeResponseNames.passport_back.toString() -> {
                val passport = Gson().fromJson(result, PassportBack::class.java)
                return getMapValueForPassportBack(passport)
            }

            DocumentTypeResponseNames.voterid_front.toString() -> {

                val voterId = Gson().fromJson(result, VoterIdFront::class.java)
                return getMapValueForVoterIdFront(voterId)
            }

            DocumentTypeResponseNames.voterid_front_new.toString() -> {
                val voterId = Gson().fromJson(result, VoterIdFrontNew::class.java)
                return getMapValueForVoterIdFrontNew(voterId)
            }

            DocumentTypeResponseNames.voterid_back.toString() -> {
                val voterId = Gson().fromJson(result, VoterIdBack::class.java)
                return getMapValueForVoterIdBack(voterId)
            }

            else -> {
                return null
            }
        }
    }

    /**
     * TODO Retrieve
     *
     * @param aadhaarFrontBottom object
     * @return hashmap
     */

    private fun getMapValueForAadharFrontBottom(aadhaarFrontBottom: AadhaarFrontBottom?): java.util.HashMap<String, String>? {

        val aadharFrontBottomMap = HashMap<String,String>()
        aadharFrontBottomMap["Name"] = aadhaarFrontBottom?.name?.value.toString()
        aadharFrontBottomMap["Aadhar"] = aadhaarFrontBottom?.aadhar?.value.toString()
        aadharFrontBottomMap["Gender"] = aadhaarFrontBottom?.gender?.value.toString()
        aadharFrontBottomMap["Dob"] = aadhaarFrontBottom?.dob?.value.toString()
        aadharFrontBottomMap["Yob"] = aadhaarFrontBottom?.yob?.value.toString()
        aadharFrontBottomMap["Father"] = aadhaarFrontBottom?.father?.value.toString()
        aadharFrontBottomMap["Mother"] = aadhaarFrontBottom?.mother?.value.toString()
        return aadharFrontBottomMap
    }


    /**
     * TODO Retrieve
     *
     * @param pan object
     * @return hashmap
     */
    private fun getMapValueforPan(pan: Pan?): HashMap<String, String> {
        val panMap = HashMap<String,String>()
        panMap["Pan Number"] = pan?.panNumber?.value.toString()
        panMap["Name"] = pan?.name?.value.toString()
        panMap["Date of Issue"] = pan?.dateOfIssue?.value.toString()
        panMap["Date"] = pan?.date?.value.toString()
        panMap["Father"] = pan?.father?.value.toString()
        return panMap
    }


    /**
     * TODO Retrieve
     *
     * @param pan old object
     * @return hashmap
     */
    private fun getMapValueforOldPan(pan: OldPan?): HashMap<String, String> {
        val panMap = HashMap<String,String>()
        panMap["Pan Number"] = pan?.panNumber?.value.toString()
        panMap["Name"] = pan?.name?.value.toString()
        panMap["Date"] = pan?.date?.value.toString()
        panMap["Father"] = pan?.father?.value.toString()
        return panMap
    }


    /**
     * TODO Retrieve
     *
     * @param aadhaarBack object
     * @return hashmap
     */
    private fun getMapValueForAadharBack(aadhaarBack: AadhaarBack?): java.util.HashMap<String, String>? {

        val aadharBack = HashMap<String,String>()
        aadharBack["Aadhar"] = aadhaarBack?.aadhaar?.value.toString()
        aadharBack["Address"] = aadhaarBack?.address?.value.toString()
        aadharBack["Father"] = aadhaarBack?.father?.value.toString()
        aadharBack["Husband"] = aadhaarBack?.husband?.value.toString()
        aadharBack["Pin"] = aadhaarBack?.pin?.value.toString()
        return aadharBack
    }


    /**
     * TODO Retrieve
     *
     * @param aadhaarFrontTop object
     * @return hashmap
     */
    private fun getMapValueForAadharFrontTop(aadhaarFrontTop: AadhaarFrontTop?): java.util.HashMap<String, String>? {

        val aadharfrontTop = HashMap<String,String>()
        aadharfrontTop["Name"] = aadhaarFrontTop?.name?.value.toString()
        aadharfrontTop["Aadhar"] = aadhaarFrontTop?.aadhaar?.value.toString()
        aadharfrontTop["Father"] = aadhaarFrontTop?.father?.value.toString()
        aadharfrontTop["Husband"] = aadhaarFrontTop?.husband?.value.toString()
        aadharfrontTop["Address"] = aadhaarFrontTop?.address?.value.toString()
        aadharfrontTop["Pin"] = aadhaarFrontTop?.pin?.value.toString()
        return aadharfrontTop
    }

    /**
     * TODO Retrieve
     *
     * @param passportFront object
     * @return hashmap
     */
    private fun getMapValueForPassportFront(passportFront: PassportFront?): java.util.HashMap<String, String>? {

        val passportFrontTop  = HashMap<String,String>()
        passportFrontTop["Passport number"] = passportFront?.passportNumber?.value.toString()
        passportFrontTop["Name"] = passportFront?.givenName?.value.toString()
        passportFrontTop["Surname"] = passportFront?.surname?.value.toString()
        passportFrontTop["Dob"] = passportFront?.dob?.value.toString()
        passportFrontTop["Country code"] = passportFront?.countryCode?.value.toString()
        passportFrontTop["Doe"] = passportFront?.doe?.value.toString()
        passportFrontTop["Nationality"] = passportFront?.nationality?.value.toString()
        passportFrontTop["Place of birth"] = passportFront?.placeOfBirth?.value.toString()
        passportFrontTop["Place of issue"] = passportFront?.placeOfIssue?.value.toString()
        passportFrontTop["Type"] = passportFront?.type?.value.toString()
        return passportFrontTop
    }

    /**
     * TODO Retrieve
     *
     * @param passportBack object
     * @return hashmap
     */
    private fun getMapValueForPassportBack(passportBack: PassportBack?): java.util.HashMap<String, String>? {

        val passportBackMap  = HashMap<String,String>()
        passportBackMap["Passport number"] = passportBack?.passportNumber?.value.toString()
        passportBackMap["Address"] = passportBack?.address?.value.toString()
        passportBackMap["Father"] = passportBack?.father?.value.toString()
        passportBackMap["File number"] = passportBack?.fileNumber?.value.toString()
        passportBackMap["Mother"] = passportBack?.mother?.value.toString()
        passportBackMap["Old doi"] = passportBack?.oldDoi?.value.toString()
        passportBackMap["Old passport number"] = passportBack?.oldPassportNumber?.value.toString()
        passportBackMap["Old place of issue"] = passportBack?.oldPlaceOfIssue?.value.toString()
        passportBackMap["Pin"] = passportBack?.pin?.value.toString()
        passportBackMap["Spouse"] = passportBack?.spouse?.value.toString()
        return passportBackMap
    }


    /**
     * TODO Retrieve
     *
     * @param voterIdFront object
     * @return hashmap
     */
    private fun getMapValueForVoterIdFront(voterIdFront: VoterIdFront?): java.util.HashMap<String, String>? {

        val voterIDFrontMap  = HashMap<String,String>()
        voterIDFrontMap["Name"] = voterIdFront?.name?.value.toString()
        voterIDFrontMap["Voter id"] = voterIdFront?.voterid?.value.toString()
        voterIDFrontMap["Dob"] = voterIdFront?.dob?.value.toString()
        voterIDFrontMap["Gender"] = voterIdFront?.gender?.value.toString()
        voterIDFrontMap["Doc"] = voterIdFront?.doc?.value.toString()
        voterIDFrontMap["Relation"] = voterIdFront?.relation?.value.toString()
        voterIDFrontMap["Age"] = voterIdFront?.age?.value.toString()
        return voterIDFrontMap
    }


    /**
     * TODO Retrieve
     *
     * @param voterIdFrontNew object
     * @return hashmap
     */
    private fun getMapValueForVoterIdFrontNew(voterIdFrontNew: VoterIdFrontNew?): java.util.HashMap<String, String>? {

        val voterIdFrontNewMap  = HashMap<String,String>()
        voterIdFrontNewMap["Name"] = voterIdFrontNew?.name?.value.toString()
        voterIdFrontNewMap["Voter id"] = voterIdFrontNew?.voterid?.value.toString()
        voterIdFrontNewMap["Relation"] = voterIdFrontNew?.relation?.value.toString()
        return voterIdFrontNewMap
    }


    /**
     * TODO Retrieve
     *
     * @param voterIdFront object
     * @return hashmap
     */
    private fun getMapValueForVoterIdBack(voterIdFront: VoterIdBack?): java.util.HashMap<String, String>? {

        val voterIDBackMap  = HashMap<String,String>()
        voterIDBackMap["Voter id"] = voterIdFront?.voterid?.value.toString()
        voterIDBackMap["Pin"] = voterIdFront?.pin?.value.toString()
        voterIDBackMap["Address"] = voterIdFront?.address?.value.toString()
        voterIDBackMap["Type"] = voterIdFront?.type?.value.toString()
        voterIDBackMap["Dob"] = voterIdFront?.dob?.value.toString()
        voterIDBackMap["Gender"] = voterIdFront?.gender?.value.toString()
        voterIDBackMap["Age"] = voterIdFront?.age?.value.toString()
        return voterIDBackMap
    }

    private fun getExactCardType(result: JSONArray?, index:Int):String
{
//    return (result?.getJSONArray("result")?.get(0) as JSONObject).get("type").toString()
    return ((result?.get(index) as JSONObject).getJSONArray("result").get(0) as JSONObject).get("type").toString()
    //return ((result?.getJSONArray("ocrdata")?.get(index) as JSONObject).getJSONArray("result")?.get(0) as JSONObject).get("type").toString()
}
    private fun getFaceMatchResult(result: JSONObject?):String
    {
        return (result?.getJSONObject("faceMatchData")?.getJSONObject("result").toString())
    }

    private fun getFaceMatchMap(faceObject: FaceMatchResult?): java.util.HashMap<String, String> {

        val faceMatchMap = HashMap<String,String>()
        faceMatchMap["Match"] = faceObject?.match.toString()
        faceMatchMap["Match Score"] = faceObject?.matchScore.toString()
        faceMatchMap["To Be Reviewed"] = faceObject?.toBeReviewed.toString()
        return  faceMatchMap
    }

    private fun getLivenessMap(livenessResult: LivenessResult): java.util.HashMap<String, String> {
        val livenessResultMap = HashMap<String,String>()
        livenessResultMap["Live"] = livenessResult.live?:""
        livenessResultMap["Liveness Score"] = livenessResult.livenessScore.toString()
        livenessResultMap["To Be Reviewed"] = livenessResult.toBeReviewed?:""
        return  livenessResultMap
    }
}
