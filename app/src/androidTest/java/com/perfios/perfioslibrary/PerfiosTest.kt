package com.perfios.perfioslibrary

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.perfios.interfaces.KYCResultCallBack
import com.perfios.perfios.Perfios
import org.json.JSONArray
import org.json.JSONObject
import org.junit.*
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class PerfiosTest {
    @get:Rule
    var activityRule = ActivityTestRule(LoginActivity::class.java,false,true)

    @Before
    fun init()
    {

    }

    @Test
    fun initPerfiosWithClientIdEmpty() {
        val result = startPerfiosTransaction(activityRule.activity,"","abcd1234","acme")
        Assert.assertEquals(false,result)
    }


    @Test
    fun initPerfiosWithSecretEmpty()
    {
        val result = startPerfiosTransaction(activityRule.activity,"4cdbd121-5644-11e7-8dbd-0242ac110003","","acme")
        Assert.assertEquals(false,result)
    }

    @Test
    fun initPerfiosWithOrgEmpty()
    {
        val result = startPerfiosTransaction(activityRule.activity,"4cdbd121-5644-11e7-8dbd-0242ac110003","abcd1234","")
        Assert.assertEquals(false,result)
    }

    @Test
    fun initPerfiosWithAllFeildsEmpty()
    {
        val result = startPerfiosTransaction(activityRule.activity,"","","")
        Assert.assertEquals(false,result)
    }

    @Test
    fun initPerfiosWithAllFeilds()
    {
        val result = startPerfiosTransaction(activityRule.activity,"4cdbd121-5644-11e7-8dbd-0242ac110003","abcd1234","acme")
        Assert.assertEquals(true,result)
    }

    private fun startPerfiosTransaction(context: Context, clientId:String, secret:String, org :String) : Boolean {
        /** single call to make KYC done.
        All other operations happening inside SDK are abstracted to SDK consumers.
         */
        var isSuccess = false

        val syncObject = Object()

        Perfios.init(context,clientId,secret,org,object:
            KYCResultCallBack {
            override fun onSuccess(
                ocrResult: JSONArray?,
                faceMatchResult: JSONObject?,
                livenessResult: JSONObject?,
                imageURI: String
            ) {
                //TODO : Remove log
                isSuccess = true
                synchronized (syncObject){
                    syncObject.notify()
                }
            }

            override fun onFailed(error: String, t: Throwable?) {
                //Log.d("onFailed",error)
                synchronized (syncObject){
                    syncObject.notify()
                }
            }
        })

        synchronized (syncObject){
            syncObject.wait()
        }
        return isSuccess
    }

    @After
    fun tearDown() {
    }
}
