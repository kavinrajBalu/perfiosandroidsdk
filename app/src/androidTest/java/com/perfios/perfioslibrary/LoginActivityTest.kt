package com.perfios.perfioslibrary

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import org.junit.After
import org.junit.Before

import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
internal class LoginActivityTest {
    private lateinit var stringToBetypedInUuidFeild: String
    private lateinit var stringToBetypedInSecretFeild :String
    private lateinit var stringToBetypedInOrganizationFeild : String

    @get:Rule
    var activityRule: ActivityTestRule<LoginActivity> = ActivityTestRule(LoginActivity::class.java)

    @Before
    fun setUp() {
        stringToBetypedInUuidFeild = "12345"
        stringToBetypedInSecretFeild = "perfios@123"
        stringToBetypedInOrganizationFeild = "Axsis"
    }

    @Test
    fun validateFields()
    {
        //enter values in all the fields
        onView(withId(R.id.uuid_edit_text)).perform(typeText(stringToBetypedInUuidFeild), closeSoftKeyboard())

        onView(withId(R.id.secret_edit_text)).perform(typeText(stringToBetypedInSecretFeild), closeSoftKeyboard())

        onView(withId(R.id.org_edit_text)).perform(typeText(stringToBetypedInOrganizationFeild), closeSoftKeyboard())

        //click init button
        onView(withId(R.id.login_button)).perform(click())
    }

    @After
    fun tearDown() {
    }
}